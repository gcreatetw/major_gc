# FluxStore - the universal Woocommerce Magento Opencart mobile app by Flutter
- Document & Support Ticket: https://support.inspireui.com
- Blog & Changelog: https://medium.com/@inspireui
- Youtube video guide: http://youtube.com/inspireui
- Website: https://fluxstore.app
- Company website: https://inspireui.com
- Fuxbuilder website: https://fluxbuilder.com

## Download package included:
- Flutter project: (open current folder by Android Studio or VS Code)
- Figma design file: resource/Fluxstore.fig
- Animate splashscreen file: resource/splashscreen.riv (https://rive.app)
- Firebase extension: resource/Firebase Functions
- Magento extension: resource/Magento-extension
- Opencart extension: resource/Opencart-extension
- Wordpress plugins: resource/Wordpress-extension
- Free WooCommerce site: http://github.com/inspireui/mstore

## Download free Fluxbuilder:
- Fluxbuilder is available on following device: Windows 10 and Mac OS
- Download the latest version https://github.com/inspireui/fluxbuilder/releases


##environment
-flutter version 2.8.1

# change
##drawer
- custom drawer list
  
##homepage
- custom homepage layout



##change log
-v0.2.0
custom drawer list
add banner
add category image in homepage
change bottom navigation bar icon
remove social login
change 出售 to -
remove origin category at homepage
remove black friday banner
change setting background image
change splash image
add url
change product image ratio
-v0.3.0
fix cart start shopping bug
remove price dot
category pull to refresh
add drawer at category
change category search
remove search QR code
can search category and product
add credit at cart
change checkout UI
add consumer card type
change bundle id to tw.major.mjhealth.test
change to locale
-v0.3.1
fix payment page not showing
add mj card
add note
change pay method
checkout CartQuantity will change
category can sort by description
modify card type
add address choose
remove shipping method every time
-v0.3.2
fix payment not login
auto fill checkout data
-v0.4.0
change MJUser api
change bottom navigation bar layout
change home page UI
-v0.5.0
improve product card
improve background and reduce space under blog
finish sso login
remove category exclude 30 31
change logo
finish auto fill cart info
temp remove
remove share
add version name
-v0.5.1
fix ios can't login bug
-v0.5.2
MJ Premium and MJ Select can't buy at the same time
change text
block not get payment method
change home page top to iframe
-v0.5.3
finish bill
fix home page layout
remove welcome page
change homepage iframe ratio
-v0.5.4
fix block diff category error
change home page iframe ratio
change order history list item layout
-v0.5.5
change add address format
can add note when checkout
-v0.5.6
remove ios location request
block unpurchasable product
-v0.5.7
fix can't add product bug
text size won't change by the device setting
change privacy url
change home page layout
fix product title english and chinese have different height
will load every banner
add card number param 
-v0.5.9
change homepage iframe ratio
add finish payment loading
finish judge payment status
change banner api
banner will reload when refresh
-v0.5.10
welcome text will change according time
fix logo error
-v0.5.11
change point to purchase info
modify homepage webview
-v0.5.12
user info will load every times
add webview load error
homepage can refresh blog
change setting position
-v0.5.13
fix uuid showing bug
add reverse check is login
-v0.6.0
change history card ui
change order history detail screen UI
-v0.6.1
change smart chat
auto fill card number
add order ask
-v0.6.2
payment now can go back
add judge phone format error
change setting screen user name
now will get last shipping address
add single delivery state
change report url
banner can click
change order history layout
-0.6.3
change heal cloud url
-0.6.6
remove blog author name
ensure shop is login secretly
update recent product info
can search by tag
remove reserve health appbar and remove author
product orderby menu_order
add get url api and UAT check and version check(UAT in env.dart,search "check version" to find where the version check)