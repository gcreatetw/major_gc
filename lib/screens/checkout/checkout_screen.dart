import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools/tools.dart';
import '../../custom/models/bill_model.dart';
import '../../custom/models/mj_user_info_model.dart';
import '../../env.dart';
import '../../generated/l10n.dart';
import '../../models/booking/booking_model.dart';
import '../../models/index.dart'
    show
        Address,
        CartModel,
        Order,
        PaymentMethodModel,
        ShippingMethodModel,
        UserModel;
import '../../models/tera_wallet/wallet_model.dart';
import '../../services/index.dart';
import '../../widgets/product/product_bottom_sheet.dart';
import '../base_screen.dart';
import 'bill_screen.dart';
import 'payment_webview_screen.dart';
import 'review_screen.dart';
import 'widgets/custom_checkout_main.dart';
import 'widgets/payment_methods.dart';
import 'widgets/success.dart';

class CheckoutArgument {
  final bool? isModal;

  const CheckoutArgument({this.isModal});
}

class Checkout extends StatefulWidget {
  final bool? isModal;

  const Checkout({this.isModal});

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends BaseScreen<Checkout> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int tabIndex = 0;
  Order? newOrder;
  int? newOrderStatus;
  bool isPayment = false;
  bool isLoading = false;
  bool enabledShipping = kPaymentConfig['EnableShipping'];
  bool isPaying = false;

  List<Address?> listAddress = [];

  TextEditingController noteController = TextEditingController();

  Future<void> getDatafromLocal() async {
    final storage = LocalStorage(LocalStorageKey.address);
    var _list = <Address?>[];
    try {
      final ready = await storage.ready;
      if (ready) {
        var data = storage.getItem('data');
        if (data != null) {
          for (var item in data as List) {
            final add = Address.fromLocalJson(item);
            _list.add(add);
          }
        }
      }
      setState(() {
        listAddress = _list;
      });
    } catch (_) {}
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      setState(() {
        isLoading = true;
      });
      final cartModel = Provider.of<CartModel>(context, listen: false);
      final shippingMethod =
          Provider.of<ShippingMethodModel>(context, listen: false);
      final paymentMethodModel =
          Provider.of<PaymentMethodModel>(context, listen: false);
      final userModel = Provider.of<UserModel>(context, listen: false);
      cartModel.setAddress(Address(country: 'TW'));
      if (cartModel.shippingMethod == null) {
        await shippingMethod
            .getShippingMethods(
              cartModel: cartModel,
              token: Provider.of<UserModel>(context, listen: false).user != null
                  ? Provider.of<UserModel>(context, listen: false).user!.cookie
                  : null,
            )
            .then((value) => {
                  if (shippingMethod.shippingMethods != null &&
                      shippingMethod.shippingMethods!.isNotEmpty &&
                      cartModel.shippingMethod == null)
                    {
                      cartModel
                          .setShippingMethod(shippingMethod.shippingMethods![0])
                    }
                });
      } else {
        await shippingMethod.getShippingMethods(
          cartModel: cartModel,
          token: Provider.of<UserModel>(context, listen: false).user != null
              ? Provider.of<UserModel>(context, listen: false).user!.cookie
              : null,
        );
        if (shippingMethod.shippingMethods == null ||
            shippingMethod.shippingMethods!.isEmpty) {
          cartModel.setShippingMethod(null);
        } else {
          if (shippingMethod.shippingMethods?.indexWhere(
                  (element) => element.id == cartModel.shippingMethod!.id) ==
              -1) {
            cartModel.setShippingMethod(shippingMethod.shippingMethods![0]);
          }
        }
      }

      await getDatafromLocal().then((value) => {
            if (listAddress.isNotEmpty) {cartModel.setAddress(listAddress[0])}
          });
      if (cartModel.paymentMethod == null) {
        await paymentMethodModel
            .getPaymentMethods(
                cartModel: cartModel,
                shippingMethod: cartModel.shippingMethod,
                token: userModel.user != null ? userModel.user!.cookie : null)
            .then((value) => {
                  if (paymentMethodModel.paymentMethods.isNotEmpty)
                    {
                      cartModel.setPaymentMethod(
                          paymentMethodModel.paymentMethods[0])
                    }
                });
      } else {
        await paymentMethodModel.getPaymentMethods(
            cartModel: cartModel,
            shippingMethod: cartModel.shippingMethod,
            token: userModel.user != null ? userModel.user!.cookie : null);
        if (paymentMethodModel.paymentMethods.isEmpty) {
          cartModel.setPaymentMethod(null);
        } else {
          if (paymentMethodModel.paymentMethods.indexWhere(
                  (element) => element.id == cartModel.paymentMethod!.id) ==
              -1) {
            cartModel.setPaymentMethod(paymentMethodModel.paymentMethods[0]);
          }
        }
      }
      await readCartInfo();
      setState(() {
        enabledShipping = cartModel.isEnabledShipping();
        isLoading = false;
      });
    });
  }

  void setLoading(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (!kPaymentConfig['EnableAddress']) {
      setState(() {
        tabIndex = 1;
      });
      if (!enabledShipping) {
        setState(() {
          tabIndex = 2;
        });
        if (!kPaymentConfig['EnableReview']) {
          setState(() {
            tabIndex = 3;
            isPayment = true;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // Widget progressBar = Row(
    //   children: <Widget>[
    //     kPaymentConfig['EnableAddress']
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 setState(() {
    //                   tabIndex = 0;
    //                 });
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).address.toUpperCase(),
    //                       style: TextStyle(
    //                           color: tabIndex == 0
    //                               ? Theme.of(context).primaryColor
    //                               : Theme.of(context).colorScheme.secondary,
    //                           fontSize: 12,
    //                           fontWeight: FontWeight.bold),
    //                     ),
    //                   ),
    //                   tabIndex >= 0
    //                       ? ClipRRect(
    //                           borderRadius: const BorderRadius.only(
    //                               topLeft: Radius.circular(2.0),
    //                               bottomLeft: Radius.circular(2.0)),
    //                           child: Container(
    //                               height: 3.0,
    //                               color: Theme.of(context).primaryColor),
    //                         )
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     enabledShipping
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 // if (cartModel.address != null &&
    //                 //     cartModel.address!.isValid()) {
    //                 //   setState(() {
    //                 //     tabIndex = 1;
    //                 //   });
    //                 // }
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).shipping.toUpperCase(),
    //                       style: TextStyle(
    //                           color: tabIndex == 1
    //                               ? Theme.of(context).primaryColor
    //                               : Theme.of(context).colorScheme.secondary,
    //                           fontSize: 12,
    //                           fontWeight: FontWeight.bold),
    //                     ),
    //                   ),
    //                   tabIndex >= 1
    //                       ? Container(
    //                           height: 3.0,
    //                           color: Theme.of(context).primaryColor)
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     kPaymentConfig['EnableReview']
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 // if (cartModel.shippingMethod != null) {
    //                 //   setState(() {
    //                 //     tabIndex = 2;
    //                 //   });
    //                 // }
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).review.toUpperCase(),
    //                       style: TextStyle(
    //                         color: tabIndex == 2
    //                             ? Theme.of(context).primaryColor
    //                             : Theme.of(context).colorScheme.secondary,
    //                         fontSize: 12,
    //                         fontWeight: FontWeight.bold,
    //                       ),
    //                     ),
    //                   ),
    //                   tabIndex >= 2
    //                       ? Container(
    //                           height: 3.0,
    //                           color: Theme.of(context).primaryColor)
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     Expanded(
    //       child: GestureDetector(
    //         onTap: () {
    //           // if (cartModel.shippingMethod != null) {
    //           //   setState(() {
    //           //     tabIndex = 3;
    //           //   });
    //           // }
    //         },
    //         child: Column(
    //           children: <Widget>[
    //             Padding(
    //               padding: const EdgeInsets.symmetric(vertical: 13),
    //               child: Text(
    //                 S.of(context).payment.toUpperCase(),
    //                 style: TextStyle(
    //                   color: tabIndex == 3
    //                       ? Theme.of(context).primaryColor
    //                       : Theme.of(context).colorScheme.secondary,
    //                   fontSize: 12,
    //                   fontWeight: FontWeight.bold,
    //                 ),
    //               ),
    //             ),
    //             tabIndex >= 3
    //                 ? ClipRRect(
    //                     borderRadius: const BorderRadius.only(
    //                         topRight: Radius.circular(2.0),
    //                         bottomRight: Radius.circular(2.0)),
    //                     child: Container(
    //                         height: 3.0, color: Theme.of(context).primaryColor),
    //                   )
    //                 : Divider(
    //                     height: 2,
    //                     color: Theme.of(context).colorScheme.secondary)
    //           ],
    //         ),
    //       ),
    //     )
    //   ],
    // );

    return WillPopScope(
      onWillPop: () async {
        if (tabIndex != 0) {
          setState(() {
            tabIndex = 0;
          });
          return false;
        }
        return true;
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Stack(
          children: <Widget>[
            Scaffold(
              appBar: AppBar(
                leading: IconButton(
                  onPressed: () {
                    if (tabIndex != 0) {
                      setState(() {
                        tabIndex = 0;
                      });
                      return;
                    }
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back_outlined),
                ),
                automaticallyImplyLeading: false,
                backgroundColor: Theme.of(context).backgroundColor,
                title: Text(
                  S.of(context).checkout,
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                actions: <Widget>[
                  if (widget.isModal != null && widget.isModal == true)
                    IconButton(
                      icon: const Icon(Icons.close, size: 24),
                      onPressed: () {
                        if (Navigator.of(context).canPop()) {
                          Navigator.popUntil(
                              context, (Route<dynamic> route) => route.isFirst);
                        } else {
                          ExpandingBottomSheet.of(context, isNullOk: true)
                              ?.close();
                        }
                      },
                    ),
                ],
              ),
              body: Builder(
                  builder: (context) => SafeArea(
                        bottom: false,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 0),
                                child: newOrder != null
                                    ? OrderedSuccess(
                                        order: newOrder,
                                        orderStatus: newOrderStatus)
                                    : Column(
                                        children: <Widget>[
                                          // !isPayment ? progressBar : Container(),
                                          Expanded(
                                            child: ListView(
                                              key: const Key(
                                                  'checkOutScreenListView'),
                                              padding: const EdgeInsets.only(
                                                  top: 10, bottom: 10),
                                              children: <Widget>[
                                                renderContent()
                                              ],
                                            ),
                                          ),
                                          if (tabIndex == 0)
                                            ElevatedButton(
                                              onPressed: () async {
                                                placeOrder(
                                                  context,
                                                  Provider.of<CartModel>(
                                                      context,
                                                      listen: false),
                                                );
                                              },
                                              style: ButtonStyle(
                                                  padding: MaterialStateProperty
                                                      .all<EdgeInsets>(
                                                          const EdgeInsets.all(
                                                              16)),
                                                  shape:
                                                      MaterialStateProperty.all<
                                                          RoundedRectangleBorder>(
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.zero),
                                                  ),
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.teal)),
                                              child: SizedBox(
                                                width: double.maxFinite,
                                                child: Text(
                                                  S.of(context).checkout,
                                                  textAlign: TextAlign.center,
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                              ),
                            )
                          ],
                        ),
                      )),
            ),
            isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white.withOpacity(0.36),
                    child: kLoadingWidget(context),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Widget renderContent() {
    switch (tabIndex) {
      case 0:
        return CustomCheckoutMain(
          noteController: noteController,
          toShipping: () {
            goToShippingTab(false);
          },
          toReceipt: () async {
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => BillScreen(
                  setBill: (BillModel bill) {},
                ),
              ),
            );
            setState(() {});
          },
          toPayment: () {
            goToPaymentTab(false);
          },
        );
      // return ShippingAddress(onNext: () {
      //   Future.delayed(Duration.zero, goToShippingTab);
      // });
      case 1:
        return Services().widget.renderShippingMethods(context, onBack: () {
          goToAddressTab(true);
        }, onNext: () {
          goToAddressTab(true);
          // goToReviewTab();
        });
      case 2:
        return ReviewScreen(onBack: () {
          goToShippingTab(true);
        }, onNext: () {
          goToPaymentTab();
        });
      case 3:
      default:
        return PaymentMethods(
            onBack: () {
              goToAddressTab(true);
            },
            onFinish: (order) async {
              setState(() {
                newOrder = order;
              });
              Provider.of<CartModel>(context, listen: false).clearCart();
              unawaited(context.read<WalletModel>().refreshWallet());
              await Services().widget.updateOrderAfterCheckout(context, order);
            },
            onLoading: setLoading);
    }
  }

  void onFinish(order, {int? orderStatus}) async {
    setState(() {
      newOrder = order;
      newOrderStatus = orderStatus;
    });
    Provider.of<CartModel>(context, listen: false).clearCart();
    unawaited(context.read<WalletModel>().refreshWallet());
    await Services().widget.updateOrderAfterCheckout(context, order);
  }

  /// tabIndex: 0
  void goToAddressTab([bool isGoingBack = false]) {
    if (kPaymentConfig['EnableAddress']) {
      setState(() {
        tabIndex = 0;
      });
    } else {
      if (!isGoingBack) {
        goToShippingTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 1
  void goToShippingTab([bool isGoingBack = false]) {
    if (enabledShipping) {
      setState(() {
        tabIndex = 1;
      });
    } else {
      if (isGoingBack) {
        goToAddressTab(isGoingBack);
      } else {
        goToReviewTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 2
  void goToReviewTab([bool isGoingBack = false]) {
    if (kPaymentConfig['EnableReview'] ?? true) {
      setState(() {
        tabIndex = 2;
      });
    } else {
      if (isGoingBack) {
        goToShippingTab(isGoingBack);
      } else {
        goToPaymentTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 3
  void goToPaymentTab([bool isGoingBack = false]) {
    if (!isGoingBack) {
      setState(() {
        tabIndex = 3;
      });
    }
  }

  ///place order
  void placeOrder(BuildContext context, CartModel cartModel) {
    if (cartModel.paymentMethod != null &&
        cartModel.shippingMethod != null &&
        cartModel.billModel != null &&
        cartModel.address != null &&
        cartModel.address!.street != null) {
      setLoading(true);
      isPaying = true;
      final paymentMethod = cartModel.paymentMethod!;
      var isSubscriptionProduct = cartModel.item.values.firstWhere(
              (element) =>
                  element?.type == 'variable-subscription' ||
                  element?.type == 'subscription',
              orElse: () => null) !=
          null;
      // Provider.of<CartModel>(context, listen: false)
      //     .setPaymentMethod(paymentMethod);

      storeCartInfo();

      /// Use Native payment

      /// Direct bank transfer (BACS)
      if (!isSubscriptionProduct && paymentMethod.id!.contains('bacs')) {
        setLoading(false);
        isPaying = false;

        showModalBottomSheet(
            context: context,
            builder: (sContext) => Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.of(context).pop(),
                            child: Text(
                              S.of(context).cancel,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption!
                                  .copyWith(color: Colors.red),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(
                        paymentMethod.description!,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      const Expanded(child: SizedBox(height: 10)),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                          setLoading(true);
                          isPaying = true;
                          Services().widget.placeOrder(
                            context,
                            cartModel: cartModel,
                            onLoading: setLoading,
                            paymentMethod: paymentMethod,
                            success: (Order order) async {
                              for (var item in order.lineItems) {
                                var product =
                                    cartModel.getProductById(item.productId!);
                                if (product?.bookingInfo != null) {
                                  product!.bookingInfo!.idOrder = order.id;
                                  var booking =
                                      await createBooking(product.bookingInfo)!;

                                  Tools.showSnackBar(
                                      Scaffold.of(context),
                                      booking
                                          ? 'Booking success!'
                                          : 'Booking error!');
                                }
                              }
                              onFinish(order, orderStatus: 1);
                              setLoading(false);
                              isPaying = false;
                            },
                            error: (message) {
                              setLoading(false);
                              if (message != null) {
                                Tools.showSnackBar(
                                    Scaffold.of(context), message);
                              }
                              isPaying = false;
                            },
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.white,
                          primary: Theme.of(context).primaryColor,
                        ),
                        child: Text(
                          S.of(context).ok,
                        ),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ));

        return;
      }

      /// PayPal Payment
      // if (!isSubscriptionProduct &&
      //     isNotBlank(kPaypalConfig['paymentMethodId']) &&
      //     paymentMethod.id!.contains(kPaypalConfig['paymentMethodId']) &&
      //     kPaypalConfig['enabled'] == true) {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //       builder: (context) => PaypalPayment(
      //         onFinish: (number) {
      //           if (number == null) {
      //             setLoading(false);
      //             isPaying = false;
      //             return;
      //           } else {
      //             createOrder(paid: true).then((value) {
      //               setLoading(false);
      //               isPaying = false;
      //             });
      //           }
      //         },
      //       ),
      //     ),
      //   );
      //   return;
      // }

      /// MercadoPago payment
      // if (!isSubscriptionProduct &&
      //     isNotBlank(kMercadoPagoConfig['paymentMethodId']) &&
      //     paymentMethod.id!.contains(kMercadoPagoConfig['paymentMethodId']) &&
      //     kMercadoPagoConfig['enabled'] == true) {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //       builder: (context) => MercadoPagoPayment(
      //         onFinish: (number) {
      //           if (number == null) {
      //             setLoading(false);
      //             isPaying = false;
      //             return;
      //           } else {
      //             createOrder(paid: true).then((value) {
      //               setLoading(false);
      //               isPaying = false;
      //             });
      //           }
      //         },
      //       ),
      //     ),
      //   );
      //   return;
      // }

      /// RazorPay payment
      /// Check below link for parameters:
      /// https://razorpay.com/docs/payment-gateway/web-integration/standard/#step-2-pass-order-id-and-other-options
      // if (!isSubscriptionProduct &&
      //     paymentMethod.id!.contains(kRazorpayConfig['paymentMethodId']) &&
      //     kRazorpayConfig['enabled'] == true) {
      //   Services().api.createRazorpayOrder({
      //     'amount': (cartModel.getTotal()! * 100).toInt().toString(),
      //     'currency': cartModel.currency,
      //   }).then((value) {
      //     final _razorServices = RazorServices(
      //       amount: (cartModel.getTotal()! * 100).toInt().toString(),
      //       keyId: kRazorpayConfig['keyId'],
      //       delegate: this,
      //       orderId: value,
      //       userInfo: RazorUserInfo(
      //         email: cartModel.address!.email,
      //         phone: cartModel.address!.phoneNumber,
      //         fullName:
      //         '${cartModel.address!.firstName} ${cartModel.address!.lastName}',
      //       ),
      //     );
      //     _razorServices.openPayment(cartModel.currency!);
      //   }).catchError((e) {
      //     setLoading(false);
      //     Tools.showSnackBar(Scaffold.of(context), e);
      //     isPaying = false;
      //   });
      //   return;
      // }

      /// PayTm payment.
      /// Check below link for parameters:
      /// https://developer.paytm.com/docs/all-in-one-sdk/hybrid-apps/flutter/
      // final availablePayTm = kPayTmConfig['paymentMethodId'] != null &&
      //     (kPayTmConfig['enabled'] ?? false) &&
      //     paymentMethod.id!.contains(kPayTmConfig['paymentMethodId']);
      // if (!isSubscriptionProduct && availablePayTm) {
      //   createOrderOnWebsite(
      //       paid: false,
      //       onFinish: (Order? order) async {
      //         if (order != null) {
      //           final _paytmServices = PayTmServices(
      //             amount: cartModel.getTotal()!.toString(),
      //             orderId: order.id!,
      //             email: cartModel.address!.email,
      //           );
      //           try {
      //             await _paytmServices.openPayment();
      //             widget.onFinish!(order);
      //           } catch (e) {
      //             Tools.showSnackBar(Scaffold.of(context), e.toString());
      //             isPaying = false;
      //           }
      //         }
      //       });
      //   return;
      // }

      /// Use WebView Payment per frameworks

      if (paymentMethod.id == 'gc_tradevan') {
        Services().widget.placeOrder(
          context,
          cartModel: cartModel,
          onLoading: setLoading,
          paymentMethod: paymentMethod,
          success: (Order? order) async {
            var userModel = Provider.of<UserModel>(context, listen: false);
            int? orderStatus;
            if (order != null) {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PaymentWebview(
                    needLogin: true,
                    canGoBack: true,
                    url:
                        '${environment['serverConfig']['url']}/wc-api/tradevan_payment/?order_id=${order.id}',
                    onFinish: (number, int status) {
                      if (number != null) {
                        orderStatus = status;
                        onFinish(Order(number: number), orderStatus: status);
                      }
                      setLoading(false);
                      isPaying = false;
                    },
                  ),
                ),
              );
              for (var item in order.lineItems) {
                var product = cartModel.getProductById(item.productId!);
                if (product?.bookingInfo != null) {
                  product!.bookingInfo!.idOrder = order.id;
                  var booking = await createBooking(product.bookingInfo)!;

                  Tools.showSnackBar(Scaffold.of(context),
                      booking ? 'Booking success!' : 'Booking error!');
                }
              }
              onFinish(order, orderStatus: orderStatus);
            }
            setLoading(false);
            isPaying = false;
          },
          error: (message) {
            setLoading(false);
            if (message != null) {
              Tools.showSnackBar(Scaffold.of(context), message);
            }

            isPaying = false;
          },
        );
        return;
      }

      Services().widget.placeOrder(
        context,
        cartModel: cartModel,
        onLoading: setLoading,
        paymentMethod: paymentMethod,
        success: (Order? order) async {
          if (order != null) {
            for (var item in order.lineItems) {
              var product = cartModel.getProductById(item.productId!);
              if (product?.bookingInfo != null) {
                product!.bookingInfo!.idOrder = order.id;
                var booking = await createBooking(product.bookingInfo)!;

                Tools.showSnackBar(Scaffold.of(context),
                    booking ? 'Booking success!' : 'Booking error!');
              }
            }
            onFinish(order, orderStatus: 1);
          }
          setLoading(false);
          isPaying = false;
        },
        error: (message) {
          setLoading(false);
          if (message != null) {
            Tools.showSnackBar(Scaffold.of(context), message);
          }

          isPaying = false;
        },
      );
    } else {
      if (cartModel.shippingMethod == null) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('請選擇寄送方式'),
            action: SnackBarAction(
              label: S.of(context).close,
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          ),
        );
        return;
      }
      if (cartModel.address == null || cartModel.address!.street == null) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('請選擇寄送地址'),
            action: SnackBarAction(
              label: S.of(context).close,
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          ),
        );
        return;
      }
      if (cartModel.billModel == null) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('請選擇發票'),
            action: SnackBarAction(
              label: S.of(context).close,
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          ),
        );
        return;
      }
      if (cartModel.paymentMethod == null) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('請選擇付款方式'),
            action: SnackBarAction(
              label: S.of(context).close,
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          ),
        );
        return;
      }
    }
  }

  Future<bool>? createBooking(BookingModel? bookingInfo) async {
    return Services().api.createBooking(bookingInfo)!;
  }

  Future<void> storeCartInfo() async {
    final cartModel = Provider.of<CartModel>(context, listen: false);
    var mjUserModel = Provider.of<MJUserInfo>(context, listen: false);
    final _storage = injector<LocalStorage>();
    await _storage.setItem('shippingMethod', cartModel.shippingMethod?.id);
    await _storage.setItem('paymentMethod', cartModel.paymentMethod?.id);
    await _storage.setItem(
        'billModel', BillModel().toJson(cartModel.billModel!));
    await _storage.setItem('userCard', cartModel.cardNum);
    await _storage.setItem('lastShippingAddress', cartModel.address);
    // await _storage.setItem(
    //     'userCard',
    //     mjUserModel
    //         .mjUserInfoModel!.cardtype![mjUserModel.cardIndex ?? 0].custno!);
  }

  Future<void> readCartInfo() async {
    final cartModel = Provider.of<CartModel>(context, listen: false);

    final paymentMethodModel =
        Provider.of<PaymentMethodModel>(context, listen: false);
    final shippingMethod =
        Provider.of<ShippingMethodModel>(context, listen: false);
    final _storage = injector<LocalStorage>();
    var shippingId = _storage.getItem('shippingMethod');
    var paymentId = _storage.getItem('paymentMethod');
    var billRaw = _storage.getItem('billModel');
    if (billRaw != null) {
      var bill = BillModel().fromJson(billRaw);
      cartModel.setBill(bill);
    }
    var addressRaw = _storage.getItem('lastShippingAddress');
    if(addressRaw != null){
      var address =
      Address.fromLocalJson(_storage.getItem('lastShippingAddress'));
      if (listAddress.isNotEmpty) {
        if (listAddress.indexWhere(
                (element) => element.toString() == address.toString()) !=
            -1) {
          cartModel.setAddress(address);
        }
      }
    }


    if (shippingMethod.shippingMethods
            ?.indexWhere((element) => element.id == shippingId) !=
        -1) {
      cartModel.setShippingMethod(shippingMethod.shippingMethods
          ?.firstWhere((element) => element.id == shippingId));
    }
    if (paymentMethodModel.paymentMethods
            .indexWhere((element) => element.id == paymentId) !=
        -1) {
      cartModel.setPaymentMethod(paymentMethodModel.paymentMethods
          .firstWhere((element) => element.id == paymentId));
    }
  }
// Future<void> createOrder(
//     {paid = false, bacs = false, cod = false, transactionId = ''}) async {
//   await createOrderOnWebsite(
//       paid: paid,
//       bacs: bacs,
//       cod: cod,
//       transactionId: transactionId,
//       onFinish: (Order? order) async {
//         if (!transactionId.toString().isEmptyOrNull && order != null) {
//           await Services()
//               .api
//               .updateOrderIdForRazorpay(transactionId, order.number);
//         }
//         onFinish(order);
//       });
// }
//
// Future<void> createOrderOnWebsite(
//     {paid = false,
//       bacs = false,
//       cod = false,
//       transactionId = '',
//       required Function(Order?) onFinish}) async {
//   setLoading(true);
//   await Services().widget.createOrder(
//     context,
//     paid: paid,
//     cod: cod,
//     bacs: bacs,
//     transactionId: transactionId,
//     onLoading: setLoading,
//     success: onFinish,
//     error: (message) {
//       Tools.showSnackBar(Scaffold.of(context), message);
//     },
//   );
//   setLoading(false);
// }
}
