import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:fstore/common/constants.dart';
import 'package:fstore/custom/constant/constants.dart';
import 'package:fstore/models/entities/address.dart';
import 'package:localstorage/localstorage.dart';

class AddAddressScreen extends StatefulWidget {
  const AddAddressScreen({Key? key}) : super(key: key);

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  final _formKey = GlobalKey<FormState>();

  bool cityNotFill = false;
  bool townNotFill = false;

  bool? phoneCorrect;

  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _zipController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();

  final _lastNameNode = FocusNode();
  final _firstNameNode = FocusNode();
  final _phoneNode = FocusNode();
  final _cityNode = FocusNode();
  final _streetNode = FocusNode();
  final _zipNode = FocusNode();
  final _countryNode = FocusNode();

  Address address = Address();
  List<Country>? countries = [];
  List<dynamic> states = [];

  Future<void> saveDataToLocal() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      if (address.state == null || address.city == null) {
        if (address.city == null) {
          setState(() {
            townNotFill = true;
          });
        } else {
          setState(() {
            townNotFill = false;
          });
        }
        if (address.state == null) {
          setState(() {
            cityNotFill = true;
          });
        } else {
          setState(() {
            cityNotFill = false;
          });
        }
        return;
      }

      setState(() {
        townNotFill = false;
      });

      setState(() {
        cityNotFill = false;
      });
      if (phoneCorrect != true) {
        return;
      }
      address.lastName = _lastNameController.text;
      address.firstName = _firstNameController.text;
      address.phoneNumber = _phoneController.text;
      address.country = 'TW';
      address.street = _streetController.text;
      address.zipCode = _zipController.text;

      final storage = LocalStorage(LocalStorageKey.address);
      var _list = <Address?>[];
      _list.add(address);
      try {
        final ready = await storage.ready;
        if (ready) {
          var data = storage.getItem('data');
          if (data != null) {
            var _data = data as List;
            for (var item in _data) {
              final add = Address.fromLocalJson(item);
              _list.add(add);
            }
          }
          await storage.setItem(
              'data',
              _list.map((item) {
                return item!.toJsonEncodable();
              }).toList());
          Navigator.of(context).pop(true);
        }
      } catch (err) {
        printLog(err);
      }
    } else {
      if (address.state == null || address.city == null) {
        if (address.city == null) {
          setState(() {
            townNotFill = true;
          });
        } else {
          setState(() {
            townNotFill = false;
          });
        }
        if (address.state == null) {
          setState(() {
            cityNotFill = true;
          });
        } else {
          setState(() {
            cityNotFill = false;
          });
        }
        return;
      }
      setState(() {
        townNotFill = false;
      });

      setState(() {
        cityNotFill = false;
      });
      if (phoneCorrect != true) {
        return;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _countryController.text = '台灣';
  }

  @override
  void dispose() {
    _lastNameController.dispose();
    _firstNameController.dispose();
    _phoneController.dispose();
    _cityController.dispose();
    _streetController.dispose();
    _zipController.dispose();
    _countryController.dispose();

    _lastNameNode.dispose();
    _firstNameNode.dispose();
    _phoneNode.dispose();
    _cityNode.dispose();
    _streetNode.dispose();
    _zipNode.dispose();
    _countryNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            '新增收件資訊',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: AutofillGroup(
                    child: Column(
                      children: [
                        const SizedBox(height: 8),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: const Text('收件資訊'),
                        ),
                        const SizedBox(height: 8),
                        // textField(
                        //     controller: _lastNameController,
                        //     node: _lastNameNode,
                        //     nextNode: _firstNameNode,
                        //     hint: '收件人姓氏'),
                        textField(
                            controller: _firstNameController,
                            node: _firstNameNode,
                            nextNode: _phoneNode,
                            hint: '收件人姓名'),
                        textField(
                            controller: _phoneController,
                            node: _phoneNode,
                            nextNode: _streetNode,
                            textInputType: TextInputType.number,
                            hint: '收件人手機號碼',
                            phone: true),
                        textField(
                            controller: _countryController,
                            node: _countryNode,
                            nextNode: _firstNameNode,
                            hint: '國家',
                            enable: false),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: renderStateInput(),
                        ),
                        if (cityNotFill)
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: const Text(
                              '*此為必填欄位',
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: renderTownInput(),
                        ),
                        if (townNotFill)
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: const Text(
                              '*此為必填欄位',
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                        textField(
                            controller: _streetController,
                            node: _streetNode,
                            nextNode: _zipNode,
                            hint: '收件人地址'),
                        // textField(
                        //     controller: _zipController,
                        //     node: _zipNode,
                        //     textInputType: TextInputType.number,
                        //     end: true,
                        //     hint: '郵遞區號'),
                        const SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                await saveDataToLocal();
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      const EdgeInsets.all(16)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero),
                  ),
                  backgroundColor: MaterialStateProperty.all(Colors.teal)),
              child: const SizedBox(
                width: double.maxFinite,
                child: Text(
                  '儲存',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderStateInput() {
    var items = <DropdownMenuItem>[];
    var cities = <String>[];
    for (var item in Constants.citiesWithZipCode) {
      cities.add(item['name']);
      items.add(
        DropdownMenuItem(
          value: item['name'],
          child: Text(item['name']),
        ),
      );
    }
    String? value;

    if (address.state != null && cities.contains(address.state)) {
      value = address.state;
    }

    return DropdownButton(
      items: items,
      value: value,
      onChanged: (dynamic val) {
        setState(() {
          address.state = val;
        });
      },
      isExpanded: true,
      itemHeight: 70,
      hint: const Text('縣市'),
    );
  }

  Widget renderTownInput() {
    var items = <DropdownMenuItem>[];
    var towns = <Map<String, dynamic>>[];
    var cities = <String>[];
    for (var item in Constants.citiesWithZipCode) {
      cities.add(item['name']);
    }
    if (address.state != null && cities.contains(address.state)) {
      towns = Constants.citiesWithZipCode
          .where((element) => element['name'] == address.state)
          .single['districts'];
      for (var item in towns) {
        items.add(
          DropdownMenuItem(
            value: item['name'],
            child: Text(item['name']),
          ),
        );
      }
    }

    String? value;

    if (towns.indexWhere((element) => element['name'] == address.city) != -1) {
      value = address.city;
    }

    return DropdownButton(
      items: items,
      value: value,
      onChanged: (dynamic val) {
        setState(() {
          address.city = val;
          address.zipCode =
              towns.firstWhere((element) => element['name'] == val)['zip'];
        });
      },
      isExpanded: true,
      itemHeight: 70,
      hint: const Text('鄉鎮市'),
    );
  }

  Widget textField({
    required TextEditingController controller,
    required FocusNode node,
    TextInputType? textInputType,
    FocusNode? nextNode,
    required String hint,
    bool end = false,
    bool enable = true,
    bool phone = false,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: TextFormField(
        enabled: enable,
        controller: controller,
        focusNode: node,
        textInputAction: end ? TextInputAction.done : TextInputAction.next,
        keyboardType: textInputType,
        onFieldSubmitted: (_) =>
            end ? null : FocusScope.of(context).requestFocus(nextNode),
        validator: (val) {
          return val!.isEmpty ? '*此為必填欄位' : null;
        },
        maxLength: phone ? 10 : null,
        onChanged: (v) {
          if (phone) {
            if (v.length == 10 && v[0] == '0' && v[1] == '9') {
              setState(() {
                phoneCorrect = true;
              });
            } else {
              setState(() {
                phoneCorrect = false;
              });
            }
          }
        },
        decoration: InputDecoration(
            counterText: '',
            errorText: phone
                ? phoneCorrect ?? true
                    ? null
                    : '手機號碼格式錯誤'
                : null,
            fillColor: Colors.white,
            filled: true,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
                width: 0.5,
              ),
            ),
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
                width: 0.5,
              ),
            ),
            focusedErrorBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
                width: 0.5,
              ),
            ),
            errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
                width: 0.5,
              ),
            ),
            border: InputBorder.none,
            labelText: hint),
      ),
    );
  }
}
