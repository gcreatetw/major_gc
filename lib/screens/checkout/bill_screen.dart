import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../custom/constant/constants.dart';
import '../../custom/models/bill_model.dart';
import '../../custom/models/upper_case_formatter.dart';
import '../../models/cart/cart_model.dart';

class BillScreen extends StatefulWidget {
  final Function setBill;

  const BillScreen({
    Key? key,
    required this.setBill,
  }) : super(key: key);

  @override
  _BillScreenState createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> with TickerProviderStateMixin {
  int billTypeValue = 0;

  TextEditingController uniformNumber = TextEditingController();
  TextEditingController companyName = TextEditingController();
  TextEditingController phoneCode = TextEditingController();
  TextEditingController natureCode = TextEditingController();

  FocusNode companyNode = FocusNode();

  bool correct = false;

  List<AnimationController> folderExpandController = [];

  List<Animation<double>> animation = [];

  List<bool> expand = [];

  String carrierTypeValue = '';

  final _formKeyThree = GlobalKey<FormState>();
  final _formKeyElectric = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < Constants.billConfig.length; i++) {
      folderExpandController.add(
        AnimationController(
          vsync: this,
          duration: const Duration(milliseconds: 300),
        ),
      );
      animation.add(
        CurvedAnimation(
          parent: folderExpandController[i],
          curve: Curves.fastOutSlowIn,
        ),
      );
      expand.add(false);
    }
    var cartModel = Provider.of<CartModel>(context, listen: false);
    if (cartModel.billModel != null) {
      billTypeValue = Constants.billConfig.indexWhere((element) =>
          element['invoice_types'] == cartModel.billModel!.billType);
      carrierTypeValue = cartModel.billModel!.carrierType ?? '';
      uniformNumber.text = cartModel.billModel!.uniCode ?? '';
      companyName.text = cartModel.billModel!.companyName ?? '';
      phoneCode.text = cartModel.billModel!.phoneCode ?? '';
      natureCode.text = cartModel.billModel!.natureCode ?? '';
      correct = checkInputError(phoneCode.text);
    }
    folderExpandController[billTypeValue].forward();
  }

  @override
  void dispose() {
    uniformNumber.dispose();
    companyName.dispose();
    phoneCode.dispose();
    natureCode.dispose();
    companyNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            '結帳',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    ListView.separated(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      shrinkWrap: true,
                      itemCount: Constants.billConfig.length,
                      itemBuilder: (context, index) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  billTypeValue = index;
                                });
                                for (var i = 0;
                                    i < Constants.billConfig.length;
                                    i++) {
                                  if (i == index) {
                                    folderExpandController[i].forward();
                                  } else {
                                    folderExpandController[i].reverse();
                                  }
                                }
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 20),
                                color: billTypeValue == index
                                    ? Theme.of(context).primaryColor
                                    : Colors.white,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        Constants.billTypes[index],
                                        style: TextStyle(
                                          color: billTypeValue == index
                                              ? Colors.white
                                              : null,
                                        ),
                                      ),
                                    ),
                                    if (billTypeValue == index)
                                      const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                  ],
                                ),
                              ),
                            ),
                            SizeTransition(
                              sizeFactor: animation[index],
                              axisAlignment: 1.0,
                              child: Form(
                                key: Constants.billConfig[index]
                                            ['invoice_types'] ==
                                        '3'
                                    ? _formKeyThree
                                    : null,
                                child: AutofillGroup(
                                  child: Column(
                                    children: [
                                      if (Constants.billConfig[index]
                                              ['subContent'] !=
                                          null)
                                        for (var element in Constants
                                            .billConfig[index]['subContent'])
                                          subContentChooser(element),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return const SizedBox(height: 15);
                      },
                    ),
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                var billModel = BillModel();
                var canSave = true;
                var billType = Constants.billConfig[billTypeValue];
                switch (billType['invoice_types']) {
                  case '2':
                    billModel.billType = billType['invoice_types'];
                    break;
                  case '3':
                    if (!_formKeyThree.currentState!.validate()) {
                      canSave = false;
                      break;
                    }
                    _formKeyThree.currentState!.save();
                    billModel.billType = billType['invoice_types'];
                    billModel.uniCode = uniformNumber.text;
                    billModel.companyName = companyName.text;
                    break;
                  case '4':
                    billModel.billType = billType['invoice_types'];
                    billModel.carrierType = carrierTypeValue;
                    switch (carrierTypeValue) {
                      case 'CQ0001':
                        if (!_formKeyElectric.currentState!.validate()) {
                          canSave = false;
                          break;
                        }
                        _formKeyElectric.currentState!.save();
                        billModel.natureCode = natureCode.text;
                        break;
                      case '3J0002':
                        if (!correct) {
                          canSave = false;
                          break;
                        }
                        billModel.phoneCode = phoneCode.text;
                        break;
                      default:
                        break;
                    }
                    break;
                }

                if (!canSave) {
                  return;
                }
                Provider.of<CartModel>(context, listen: false)
                    .setBill(billModel);
                Navigator.pop(context);
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      const EdgeInsets.all(16)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero),
                  ),
                  backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColor)),
              child: const SizedBox(
                width: double.maxFinite,
                child: Text(
                  '確認',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget subContentChooser(var subContentType) {
    switch (subContentType['type']) {
      case 'textField':
        switch (subContentType['id']) {
          case 'customer_identifier':
            return textField(
              controller: uniformNumber,
              hint: '統一編號',
              nextNode: companyNode,
            );
          case 'customer_name':
            return textField(
              controller: companyName,
              hint: '公司抬頭',
              end: true,
              node: companyNode,
            );
          default:
            return Container();
        }
      case 'dropDown':
        if (carrierTypeValue.isEmpty) {
          carrierTypeValue = subContentType['value'][0]['carruer_type'];
        }
        return SizedBox(
          width: double.maxFinite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: carrierTypeValue,
                  items: [
                    for (var element in subContentType['value'])
                      DropdownMenuItem(
                          value: element['carruer_type'],
                          child: Text(element['name']))
                  ],
                  onChanged: (value) {
                    setState(() {
                      carrierTypeValue = value!;
                    });
                  },
                ),
              ),
              if (carrierTypeValue == 'CQ0001')
                Form(
                  key: _formKeyElectric,
                  child: AutofillGroup(
                    child: textField(
                      controller: natureCode,
                      hint: '自然人憑證載具',
                      end: true,
                    ),
                  ),
                ),
              if (carrierTypeValue == '3J0002')
                textField(
                  controller: phoneCode,
                  hint: '手機條碼載具',
                  end: true,
                  checkError: checkInputError,
                ),
            ],
          ),
        );
      default:
        return Container();
    }
  }

  Widget textField({
    required TextEditingController controller,
    FocusNode? node,
    TextInputType? textInputType,
    FocusNode? nextNode,
    required String hint,
    bool end = false,
    bool enable = true,
    Function? checkError,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: TextFormField(
        enabled: enable,
        controller: controller,
        focusNode: node,
        textInputAction: end ? TextInputAction.done : TextInputAction.next,
        keyboardType: textInputType,
        onFieldSubmitted: (_) =>
            end ? null : FocusScope.of(context).requestFocus(nextNode),
        validator: (val) {
          return val!.isEmpty ? '*此為必填欄位' : null;
        },
        style: const TextStyle(fontSize: 14),
        onChanged: checkError != null
            ? (text) {
                setState(() {
                  correct = checkInputError(text);
                });
              }
            : null,
        maxLength: checkError != null ? 8 : null,
        textCapitalization: checkError != null
            ? TextCapitalization.characters
            : TextCapitalization.none,
        inputFormatters: checkError != null
            ? [
                UpperCaseTextFormatter(),
              ]
            : null,
        decoration: InputDecoration(
          counterText: '',
          errorText: checkError != null
              ? correct
                  ? null
                  : '請輸入正確手機條碼載具號碼'
              : null,
          fillColor: Colors.white,
          filled: true,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
          errorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
          border: InputBorder.none,
          labelText: hint,
        ),
      ),
    );
  }

  bool checkInputError(String value) {
    if (value.isEmpty || value[0] != '/') {
      return false;
    }
    if (value.length < 8) {
      return false;
    }
    return true;
  }
}
