import 'package:flutter/material.dart';
import '../../models/user_model.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../env.dart';
import '../../services/index.dart';
import '../../widgets/common/webview_inapp.dart';
import '../base_screen.dart';
// import 'payment_webview_plugin.dart';

const enableWebviewFlutter = true;

class PaymentWebview extends StatefulWidget {
  final String? url;
  final Function? onFinish;
  final Function? onClose;
  final bool canGoBack;
  final bool needLogin;

  const PaymentWebview(
      {this.onFinish,
      this.onClose,
      this.url,
      this.canGoBack = true,
      this.needLogin = false});

  @override
  State<StatefulWidget> createState() {
    return PaymentWebviewState();
  }
}

class PaymentWebviewState extends BaseScreen<PaymentWebview> {
  int selectedIndex = 1;

  WebViewController? webViewController;

  bool isLogin = false;

  void handleUrlChanged(String url) {
    print('touchme' + url);
    var userModel = Provider.of<UserModel>(context, listen: false);
    if (url.contains('/order-received/')) {
      final items = url.split('/order-received/');
      int? status;
      if (url.contains('success')) {
        var orderStatus =
            url.substring(url.indexOf('success=') + 8, url.length);

        if (orderStatus == 'c4ca4238a0b923820dcc509a6f75849b') {
          status = 1;
        }
        if (orderStatus == 'cfcd208495d565ef66e7dff9f98764da') {
          status = 0;
        }
      }
      if (items.length > 1) {
        final number = items[1].split('/')[0];
        widget.onFinish!(number, status);
        Navigator.of(context).pop();
      }
    }
    if (url.contains('checkout/success')) {
      widget.onFinish!('0');
      Navigator.of(context).pop();
    }

    // shopify url final checkout
    if (url.contains('thank_you')) {
      widget.onFinish!('0');
      Navigator.of(context).pop();
    }

    if (url.contains('/member-login/')) {
      widget.onFinish!('0');
      Navigator.of(context).pop();
    }

    if (isLogin && widget.needLogin) {
      setState(() {
        selectedIndex = 0;
      });
    }

    if (url ==
            '${environment['serverConfig']['url']}/?cookie=${EncodeUtils.encodeCookie(userModel.user!.cookie!)}' &&
        widget.needLogin) {
      setState(() {
        isLogin = true;
        webViewController!.loadUrl(widget.url!);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var checkoutMap = <dynamic, dynamic>{
      'url': '',
      'headers': <String, String>{}
    };

    if (widget.url != null) {
      checkoutMap['url'] = widget.url;
    } else {
      final paymentInfo = Services().widget.getPaymentUrl(context)!;
      checkoutMap['url'] = paymentInfo['url'];
      if (paymentInfo['headers'] != null) {
        checkoutMap['headers'] =
            Map<String, String>.from(paymentInfo['headers']);
      }
    }

    // // Enable webview payment plugin
    /// make sure to import 'payment_webview_plugin.dart';
    // return PaymentWebviewPlugin(
    //   url: checkoutMap['url'],
    //   headers: checkoutMap['headers'],
    //   onClose: widget.onClose,
    //   onFinish: widget.onFinish,
    // );

    if (!kIsWeb && (kAdvanceConfig['inAppWebView'] ?? false)) {
      return WebViewInApp(
          url: widget.url ?? '',
          title: '',
          onUrlChanged: (String? url) {
            if (url?.isNotEmpty ?? false) {
              handleUrlChanged(url!);
            }
          });
    }
    var userModel = Provider.of<UserModel>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: widget.canGoBack
            ? IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  widget.onFinish!(null,0);
                  Navigator.of(context).pop();

                  if (widget.onClose != null) {
                    widget.onClose!();
                  }
                })
            : null,
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: IndexedStack(
        index: selectedIndex,
        children: [
          WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: checkoutMap['url'],
            initialCookies: [
              WebViewCookie(
                  name: 'cookie',
                  value: userModel.user!.cookie!,
                  domain: environment['serverConfig']['url'].toString().replaceAll('https://', 'replace'))
            ],
            onWebViewCreated: widget.needLogin
                ? (controller) {
                    webViewController = controller;
                  }
                : null,
            onProgress: (progress) async {
              setState(() {
                selectedIndex = 1;
              });
              if (progress == 100) {
                setState(() {
                  selectedIndex = 0;
                });
              }
              var url = await webViewController?.currentUrl();
              if (widget.needLogin &&
                  url != null &&
                  !url.contains('QPay.WebPaySite')) {
                setState(() {
                  selectedIndex = 1;
                });
              }
            },
            onPageFinished: handleUrlChanged,
          ),
          Center(
            child: kLoadingWidget(context),
          )
        ],
      ),
    );
  }
}
