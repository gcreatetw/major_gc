import 'package:flutter/material.dart';
import 'package:fstore/custom/constant/constants.dart';
import 'package:fstore/custom/models/mj_user_info_model.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../models/index.dart' show AppModel, CartModel, Product, TaxModel;
import '../../../widgets/product/cart_item.dart';

class CustomCheckoutMain extends StatefulWidget {
  final Function? toShipping;
  final Function? toReceipt;
  final Function? toPayment;
  final TextEditingController noteController;

  const CustomCheckoutMain({
    Key? key,
    this.toShipping,
    this.toReceipt,
    this.toPayment,
    required this.noteController,
  }) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<CustomCheckoutMain> {
  @override
  Widget build(BuildContext context) {
    var cartModel = Provider.of<CartModel>(context);
    var mjUserModel = Provider.of<MJUserInfo>(context);
    return Consumer<CartModel>(builder: (context, model, child) {
      return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  ///product list
                  ...getProducts(model, context),

                  ///card number
                  const SizedBox(height: 8),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/payment/Artboard_16.png',
                          height: 26,
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(mjUserModel.mjUserInfoModel!
                              .cardtype![mjUserModel.cardIndex ?? 0].cardtype!),
                        ),
                        Text(mjUserModel.mjUserInfoModel!
                            .cardtype![mjUserModel.cardIndex ?? 0].custno!),
                      ],
                    ),
                  ),

                  ///shipping
                  const SizedBox(height: 8),
                  GestureDetector(
                    onTap: () {
                      widget.toShipping!();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/payment/Artboard_17.png',
                            height: 26,
                          ),
                          const SizedBox(width: 8),
                          const Expanded(
                            child: Text('寄送方式'),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              if (model.shippingMethod != null)
                                Text(
                                    '${model.shippingMethod?.title} \$${model.shippingMethod?.cost?.round()}'),
                              Text(
                                model.address != null &&
                                        model.address!.lastName != null &&
                                        model.address!.firstName != null
                                    ? model.address!.lastName! +
                                        model.address!.firstName!
                                    : '未選擇',
                                style: TextStyle(color: HexColor('#999999')),
                              )
                            ],
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Theme.of(context).primaryColor,
                          )
                        ],
                      ),
                    ),
                  ),

                  ///bill
                  const SizedBox(height: 2),
                  GestureDetector(
                    onTap: () {
                      widget.toReceipt!();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/payment/Artboard_18.png',
                            height: 26,
                          ),
                          const SizedBox(width: 8),
                          const Expanded(
                            child: Text('發票開立'),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(cartModel.billModel != null
                                  ? Constants.billConfig.firstWhere((element) =>
                                      element['invoice_types'] ==
                                      cartModel
                                          .billModel!.billType)['billingType']
                                  : '請選擇發票'),
                              if (cartModel.billModel != null &&
                                  cartModel.billModel!.billType == '4')
                                Text(
                                  cartModel.billModel!.carrierType == 'EG0142'
                                      ? '美兆會員載具'
                                      : cartModel.billModel!.carrierType ==
                                              'CQ0001'
                                          ? '自然人憑證載具'
                                          : cartModel.billModel!.carrierType ==
                                                  '3J0002'
                                              ? '手機條碼載具'
                                              : '',
                                  style: TextStyle(color: HexColor('#999999')),
                                )
                            ],
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Theme.of(context).primaryColor,
                          )
                        ],
                      ),
                    ),
                  ),

                  ///note
                  const SizedBox(height: 2),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/payment/Artboard_19.png',
                          height: 26,
                        ),
                        const SizedBox(width: 8),
                        const Text('備註'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: TextField(
                              controller: widget.noteController,
                              textAlign: TextAlign.end,
                              textInputAction: TextInputAction.done,
                              onChanged: (value) {
                                cartModel.setOrderNotes(value);
                              },
                              decoration: InputDecoration(
                                hintText: '要留言給我們什麼?',
                                hintStyle: TextStyle(
                                  color: HexColor('#999999'),
                                  fontSize: 16,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  ///total
                  const SizedBox(height: 8),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text('訂單金額(${model.totalCartQuantity} 商品)'),
                        ),
                        Text(
                          'NT\$${cartModel.getTotal()?.round()}',
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        )
                      ],
                    ),
                  ),

                  ///pay methods,checkout,total
                  const SizedBox(height: 8),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    color: Colors.white,
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            widget.toPayment!();
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/icons/payment/Artboard_20.png',
                                height: 26,
                              ),
                              const Expanded(
                                child: Text('付款方式'),
                              ),
                              Text(
                                cartModel.paymentMethod == null
                                    ? '請選擇付款方式'
                                    : cartModel.paymentMethod!.title!,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Theme.of(context).primaryColor,
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                '商品總金額',
                                style: TextStyle(color: HexColor('#999999')),
                              ),
                            ),
                            Text(
                              'NT\$${(cartModel.getTotal()! - cartModel.getShippingCost()!).round()}',
                              style: TextStyle(
                                color: HexColor('#999999'),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                '運費總金額',
                                style: TextStyle(color: HexColor('#999999')),
                              ),
                            ),
                            Text(
                              'NT\$${cartModel.getShippingCost()?.round()}',
                              style: TextStyle(
                                color: HexColor('#999999'),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        Container(
                          color: Theme.of(context).primaryColor,
                          height: 1,
                          width: double.maxFinite,
                        ),
                        const SizedBox(height: 16),
                        Row(
                          children: [
                            const Expanded(
                              child: Text('總付款金額'),
                            ),
                            Text(
                              'NT\$${cartModel.getTotal()?.round()}',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 100),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  List<Widget> getProducts(CartModel model, BuildContext context) {
    return model.productsInCart.keys.map(
      (key) {
        var productId = Product.cleanProductID(key);

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: ShoppingCartRow(
                addonsOptions: model.productAddonsOptionsInCart[key],
                product: model.getProductById(productId),
                variation: model.getProductVariationById(key),
                quantity: model.productsInCart[key],
                options: model.productsMetaDataInCart[key],
              ),
            ),
          ),
        );
      },
    ).toList();
  }
}
