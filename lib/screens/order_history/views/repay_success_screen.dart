import 'package:flutter/material.dart';
import 'package:fstore/models/order/order.dart';
import 'package:fstore/screens/checkout/widgets/success.dart';

class RepaySuccessScreen extends StatefulWidget {
  final Order? order;
  final int? orderStatus;

  const RepaySuccessScreen({
    Key? key,
    this.order,
    this.orderStatus,
  }) : super(key: key);

  @override
  State<RepaySuccessScreen> createState() => _RepaySuccessScreenState();
}

class _RepaySuccessScreenState extends State<RepaySuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      body: OrderedSuccess(
        order: widget.order,
        orderStatus: widget.orderStatus,
      ),
    );
  }
}
