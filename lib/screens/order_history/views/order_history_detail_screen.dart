import 'dart:async';

import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:fstore/screens/checkout/payment_webview_screen.dart';
import 'package:fstore/screens/order_history/views/repay_success_screen.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/tools.dart';
import '../../../env.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart' show AppModel, CartModel, UserModel;
import '../../../models/order/order.dart';
import '../../../modules/re_order/widgets/re_order_item_list.dart';
import '../../../services/index.dart';
import '../../../widgets/common/box_comment.dart';
import '../../../widgets/common/webview.dart';
import '../../base_screen.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/product_order_item.dart';

class OrderHistoryDetailScreen extends StatefulWidget {
  const OrderHistoryDetailScreen();

  @override
  _OrderHistoryDetailScreenState createState() =>
      _OrderHistoryDetailScreenState();
}

class _OrderHistoryDetailScreenState
    extends BaseScreen<OrderHistoryDetailScreen> {
  OrderHistoryDetailModel get orderHistoryModel =>
      Provider.of<OrderHistoryDetailModel>(context, listen: false);

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    // orderHistoryModel.getTracking();
    orderHistoryModel.getOrderNote();
  }

  void cancelOrder() {
    orderHistoryModel.cancelOrder();
  }

  void _onNavigate(context, OrderHistoryDetailModel model) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebView(
          url:
              "${afterShip['tracking_url']}/${model.order.aftershipTracking!.slug}/${model.order.aftershipTracking!.trackingNumber}",
          appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            leading: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: const Icon(Icons.arrow_back_ios),
            ),
            title: Text(S.of(context).trackingPage),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    return Consumer<OrderHistoryDetailModel>(builder: (context, model, child) {
      final order = model.order;
      return Scaffold(
        // backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Theme.of(context).colorScheme.secondary,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          actions: [
            if (order.status == OrderStatus.failed ||
                order.status == OrderStatus.pending)
              Center(
                child: GestureDetector(
                  onTap: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PaymentWebview(
                          needLogin: true,
                          canGoBack: true,
                          url:
                              '${environment['serverConfig']['url']}/wc-api/tradevan_payment/?order_id=${order.id}',
                          onFinish: (number, int status) async {
                            if (number != null) {
                              Provider.of<CartModel>(context, listen: false)
                                  .clearCart();
                              await Services()
                                  .widget
                                  .updateOrderAfterCheckout(context, order);
                              Navigator.popUntil(
                                  context, (route) => route.isFirst);
                              await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => RepaySuccessScreen(
                                    order: order,
                                    orderStatus: status,
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 5.0),
                    margin: const EdgeInsets.symmetric(horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                    child: const Text(
                      '重新付款',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    ),
                  ),
                ),
              )
          ],
          title: FittedBox(
            child: Text(
              '訂單內頁',
              style: TextStyle(color: Theme.of(context).colorScheme.secondary),
            ),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  S.of(context).orderNo + ' ${order.number}',
                ),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Builder(builder: (context) {
                  var finText = '完成';
                  switch (order.status) {
                    case OrderStatus.cancelled:
                      finText = '取消';
                      break;
                    case OrderStatus.refunded:
                      finText = '已退款';
                      break;
                    case OrderStatus.failed:
                      finText = '失敗';
                      break;
                    case OrderStatus.canceled:
                      finText = '取消';
                      break;
                    default:
                      finText = '完成';
                      break;
                  }
                  return Stack(
                    children: [
                      Row(
                        children: [
                          const SizedBox(width: 12),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(top: 12.5),
                            height: 2,
                            width: double.maxFinite,
                            color: order.status == OrderStatus.pending
                                ? Colors.grey
                                : Theme.of(context).primaryColor,
                          )),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(top: 12.5),
                            height: 2,
                            width: double.maxFinite,
                            color: order.status == OrderStatus.pending ||
                                    order.status == OrderStatus.processing
                                ? Colors.grey
                                : Theme.of(context).primaryColor,
                          )),
                          const SizedBox(width: 12),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '1',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                '待付款',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                        color: Theme.of(context).primaryColor),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: order.status == OrderStatus.pending
                                      ? Colors.grey
                                      : Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '2',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                '已付款',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                        color: order.status ==
                                                OrderStatus.pending
                                            ? Colors.grey
                                            : Theme.of(context).primaryColor),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: order.status ==
                                              OrderStatus.completed ||
                                          order.status == OrderStatus.failed ||
                                          order.status ==
                                              OrderStatus.canceled ||
                                          order.status ==
                                              OrderStatus.cancelled ||
                                          order.status == OrderStatus.refunded
                                      ? Theme.of(context).primaryColor
                                      : Colors.grey,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '3',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                finText,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                      color: order.status ==
                                                  OrderStatus.completed ||
                                              order.status ==
                                                  OrderStatus.failed ||
                                              order.status ==
                                                  OrderStatus.canceled ||
                                              order.status ==
                                                  OrderStatus.cancelled ||
                                              order.status ==
                                                  OrderStatus.refunded
                                          ? Theme.of(context).primaryColor
                                          : Colors.grey,
                                    ),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  );
                }),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Card(
                    child: ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          final _item = order.lineItems[index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: ProductOrderItem(
                              productItem: model.order.lineItems[index],
                              orderId: order.id!,
                              orderStatus: order.status!,
                              product: _item,
                              index: index,
                              storeDeliveryDates: order.storeDeliveryDates,
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              children: List.generate(
                                  150 ~/ 1,
                                  (index) => Expanded(
                                        child: Container(
                                          color: index % 2 == 0
                                              ? Colors.transparent
                                              : Colors.grey,
                                          height: 1,
                                        ),
                                      )),
                            ),
                          );
                        },
                        itemCount: order.lineItems.length)
                    // Column(
                    //   children: [
                    //     ...List.generate(
                    //       order.lineItems.length,
                    //       (index) {
                    //         final _item = order.lineItems[index];
                    //         return ProductOrderItem(
                    //           orderId: order.id!,
                    //           orderStatus: order.status!,
                    //           product: _item,
                    //           index: index,
                    //           storeDeliveryDates: order.storeDeliveryDates,
                    //         );
                    //       },
                    //     )
                    //   ],
                    // ),
                    ),
              ),
              const SizedBox(height: 16),
              Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('收件人'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billing!.firstName!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Icon(
                          Icons.phone,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('電話'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billing!.phoneNumber!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('收貨地址'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.shipping!.state! +
                                  order.shipping!.city! +
                                  order.shipping!.street!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Icon(
                          Icons.receipt_long_rounded,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('發票號碼'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.invoiceNumber ?? '',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Icon(
                          Icons.message,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('備註'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.customerNote!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('下單人'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billing!.firstName!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                  ],
                ),
              ),
              // Container(
              //   decoration: BoxDecoration(
              //     color: Theme.of(context).primaryColorLight,
              //     borderRadius: BorderRadius.circular(10.0),
              //   ),
              //   padding: const EdgeInsets.all(15),
              //   margin: const EdgeInsets.symmetric(vertical: 10),
              //   child: Column(
              //     children: <Widget>[
              //       if (order.deliveryDate != null &&
              //           order.storeDeliveryDates == null)
              //         Padding(
              //           padding: const EdgeInsets.only(bottom: 10.0),
              //           child: Row(
              //             children: <Widget>[
              //               Text(S.of(context).expectedDeliveryDate,
              //                   style: Theme.of(context)
              //                       .textTheme
              //                       .subtitle1!
              //                       .copyWith(
              //                         fontWeight: FontWeight.w400,
              //                       )),
              //               const SizedBox(width: 8),
              //               Expanded(
              //                 child: Text(
              //                   order.deliveryDate!,
              //                   textAlign: TextAlign.right,
              //                   style: Theme.of(context)
              //                       .textTheme
              //                       .subtitle1!
              //                       .copyWith(
              //                         fontWeight: FontWeight.w700,
              //                       ),
              //                 ),
              //               )
              //             ],
              //           ),
              //         ),
              //       if (order.paymentMethodTitle != null)
              //         Row(
              //           children: <Widget>[
              //             Text(S.of(context).paymentMethod,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w400,
              //                     )),
              //             const SizedBox(width: 8),
              //             Expanded(
              //               child: Text(
              //                 order.paymentMethodTitle!,
              //                 textAlign: TextAlign.right,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w700,
              //                     ),
              //               ),
              //             )
              //           ],
              //         ),
              //       if (order.paymentMethodTitle != null)
              //         const SizedBox(height: 10),
              //       (order.shippingMethodTitle != null &&
              //               kPaymentConfig['EnableShipping'])
              //           ? Row(
              //               children: <Widget>[
              //                 Text(S.of(context).shippingMethod,
              //                     style: Theme.of(context)
              //                         .textTheme
              //                         .subtitle1!
              //                         .copyWith(
              //                           fontWeight: FontWeight.w400,
              //                         )),
              //                 const SizedBox(width: 8),
              //                 Expanded(
              //                   child: Text(
              //                     order.shippingMethodTitle!,
              //                     textAlign: TextAlign.right,
              //                     style: Theme.of(context)
              //                         .textTheme
              //                         .subtitle1!
              //                         .copyWith(
              //                           fontWeight: FontWeight.w700,
              //                         ),
              //                   ),
              //                 )
              //               ],
              //             )
              //           : Container(),
              //       if (order.totalShipping != null) const SizedBox(height: 10),
              //       if (order.totalShipping != null)
              //         Row(
              //           children: <Widget>[
              //             Text(S.of(context).shipping,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w400,
              //                     )),
              //             const SizedBox(width: 8),
              //             Expanded(
              //               child: Text(
              //                 PriceTools.getCurrencyFormatted(
              //                     order.totalShipping, currencyRate)!,
              //                 textAlign: TextAlign.right,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w700,
              //                     ),
              //               ),
              //             )
              //           ],
              //         ),
              //       const SizedBox(height: 10),
              //       Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: <Widget>[
              //           Text(
              //             S.of(context).subtotal,
              //             style:
              //                 Theme.of(context).textTheme.subtitle1!.copyWith(
              //                       fontWeight: FontWeight.w400,
              //                     ),
              //           ),
              //           Text(
              //             PriceTools.getCurrencyFormatted(
              //                 order.lineItems.fold(
              //                     0,
              //                     (dynamic sum, e) =>
              //                         sum + double.parse(e.total!)),
              //                 currencyRate)!,
              //             style:
              //                 Theme.of(context).textTheme.subtitle1!.copyWith(
              //                       fontWeight: FontWeight.w700,
              //                     ),
              //           )
              //         ],
              //       ),
              //       if (order.paymentMethodTitle != null)
              //         const SizedBox(height: 10),
              //       if (order.paymentMethodTitle != null)
              //         Row(
              //           children: <Widget>[
              //             Text(S.of(context).paymentMethod,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w400,
              //                     )),
              //             const SizedBox(width: 8),
              //             Expanded(
              //               child: Text(
              //                 order.paymentMethodTitle!,
              //                 textAlign: TextAlign.right,
              //                 style: Theme.of(context)
              //                     .textTheme
              //                     .subtitle1!
              //                     .copyWith(
              //                       fontWeight: FontWeight.w700,
              //                     ),
              //               ),
              //             )
              //           ],
              //         ),
              //       const SizedBox(height: 10),
              //       Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: <Widget>[
              //           Text(
              //             S.of(context).totalTax,
              //             style:
              //                 Theme.of(context).textTheme.subtitle1?.copyWith(
              //                       fontWeight: FontWeight.w400,
              //                     ),
              //           ),
              //           OrderPrice.tax(
              //               order: order, currencyRate: currencyRate),
              //         ],
              //       ),
              //       Divider(
              //         height: 20,
              //         color: Theme.of(context).colorScheme.secondary,
              //       ),
              //       Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: <Widget>[
              //           Text(
              //             S.of(context).total,
              //             style:
              //                 Theme.of(context).textTheme.subtitle1!.copyWith(
              //                       fontWeight: FontWeight.bold,
              //                       color: Theme.of(context).primaryColor,
              //                     ),
              //           ),
              //           OrderPrice(
              //             order: order,
              //             currencyRate: currencyRate,
              //           ),
              //         ],
              //       ),
              //     ],
              //   ),
              // ),
              // if (model.order.aftershipTracking != null)
              //   Padding(
              //     padding: const EdgeInsets.only(top: 20.0),
              //     child: GestureDetector(
              //       onTap: () => _onNavigate(context, model),
              //       child: Align(
              //         alignment: Alignment.topLeft,
              //         child: Row(
              //           children: <Widget>[
              //             Text('${S.of(context).trackingNumberIs} '),
              //             Text(
              //               model.order.aftershipTracking!.trackingNumber!,
              //               style: TextStyle(
              //                 color: Theme.of(context).primaryColor,
              //                 decoration: TextDecoration.underline,
              //               ),
              //             )
              //           ],
              //         ),
              //       ),
              //     ),
              //   ),
              // Services().widget.renderOrderTimelineTracking(context, order),
              // const SizedBox(height: 20),

              /// Render the Cancel and Refund
              if (kPaymentConfig['EnableRefundCancel'])
                Services()
                    .widget
                    .renderButtons(context, order, cancelOrder, refundOrder),

              // const SizedBox(height: 20),
              // if (order.billing != null) ...[
              //   Text(S.of(context).shippingAddress,
              //       style: const TextStyle(
              //           fontSize: 18, fontWeight: FontWeight.bold)),
              //   const SizedBox(height: 10),
              //   Text(
              //     ((order.billing!.apartment?.isEmpty ?? true)
              //             ? ''
              //             : '${order.billing!.apartment} ') +
              //         ((order.billing!.block?.isEmpty ?? true)
              //             ? ''
              //             : '${(order.billing!.apartment?.isEmpty ?? true) ? '' : '- '} ${order.billing!.block}, ') +
              //         order.billing!.street! +
              //         ', ' +
              //         order.billing!.city! +
              //         ', ' +
              //         getCountryName(order.billing!.country),
              //   ),
              // ],
              if (order.status == OrderStatus.processing &&
                  kPaymentConfig['EnableRefundCancel'])
                Column(
                  children: <Widget>[
                    const SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            height: 45,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  onPrimary: Colors.white,
                                  primary: HexColor('#056C99'),
                                ),
                                onPressed: refundOrder,
                                child: Text(
                                    S.of(context).refundRequest.toUpperCase(),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w700))),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              const SizedBox(height: 16),
              if (kPaymentConfig['ShowOrderNotes'] ?? true)
                Builder(
                  builder: (context) {
                    final listOrderNote = model.listOrderNote;
                    if (listOrderNote?.isEmpty ?? true) {
                      return const SizedBox();
                    }
                    return Container(
                      color: Colors.white,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: [
                              Icon(
                                Icons.info,
                                color: Theme.of(context).primaryColor,
                              ),
                              const SizedBox(width: 4),
                              Text(
                                S.of(context).orderNotes,
                              ),
                            ],
                          ),
                          const SizedBox(height: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ...List.generate(
                                listOrderNote!.length,
                                (index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(bottom: 15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        CustomPaint(
                                          painter: BoxComment(
                                              color: Theme.of(context)
                                                  .primaryColor
                                                  .withOpacity(0.2)),
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10,
                                                  right: 10,
                                                  top: 15,
                                                  bottom: 8),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      const SizedBox(width: 16),
                                                      Expanded(
                                                        child: Linkify(
                                                          text: listOrderNote[
                                                                  index]
                                                              .note!,
                                                          style:
                                                              const TextStyle(
                                                                  fontSize: 13,
                                                                  height: 1.2),
                                                          onOpen: (link) async {
                                                            await Tools
                                                                .launchURL(
                                                                    link.url);
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      const SizedBox(width: 16),
                                                      Expanded(
                                                        child: Text(
                                                          formatTime(DateTime
                                                              .parse(listOrderNote[
                                                                      index]
                                                                  .dateCreated!)),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                            fontSize: 13,
                                                            color: Theme.of(
                                                                    context)
                                                                .colorScheme
                                                                .secondary
                                                                .withOpacity(
                                                                    0.8),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
              const SizedBox(height: 16),
              Container(
                padding: const EdgeInsets.all(16),
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '商品總金額',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 14.0,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.8),
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'NT.' +
                                  (order.total! - order.totalShipping!)
                                      .toInt()
                                      .toString(),
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '寄送方式',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 14.0,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.8),
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '(${order.shippingMethodTitle})'
                                      'NT.' +
                                  order.totalShipping!.toInt().toString(),
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Container(
                      width: double.maxFinite,
                      height: 1,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '總共',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 16.0,
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'NT.' + order.total!.toInt().toString(),
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 16.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          _reOrder(order);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Theme.of(context).primaryColor),
                          ),
                          child: Text(
                            '再次下單',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return WebView(
                              url:
                                  '${environment['serverConfig']['url']}/return/?appstyle=1&order_number=${order.orderNumber}',
                              title: '訂單諮詢',
                            );
                          }));
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Theme.of(context).primaryColor),
                          ),
                          child: Text(
                            '訂單諮詢',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50)
            ],
          ),
        ),
      );
    });
  }

  void _reOrder(Order order) async {
    final result = await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.75,
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(10.0),
          ),
        ),
        child: ReOrderItemList(
          lineItems: order.lineItems,
        ),
      ),
    );

    if (result == true) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(S.of(context).yourOrderHasBeenAdded),
          duration: const Duration(seconds: 3),
        ),
      );
    }
  }

  String getCountryName(country) {
    try {
      return CountryPickerUtils.getCountryByIsoCode(country).name;
    } catch (err) {
      return country;
    }
  }

  Future<void> refundOrder() async {
    _showLoading();
    try {
      await orderHistoryModel.createRefund();
      _hideLoading();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderSuccess)));
    } catch (err) {
      _hideLoading();

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderFailed)));
    }
  }

  void _showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white30,
              borderRadius: BorderRadius.circular(5.0),
            ),
            padding: const EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                kLoadingWidget(context),
                // const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(S.of(context).cancel),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _hideLoading() {
    Navigator.of(context).pop();
  }

  String formatTime(DateTime time) {
    return DateFormat('dd/MM/yyyy, HH:mm').format(time);
  }
}
