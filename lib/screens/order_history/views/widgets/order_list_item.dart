import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../../common/constants.dart';
import '../../../../common/tools.dart';
import '../../../../generated/l10n.dart';
import '../../../../models/order/order.dart';
import '../../models/order_history_detail_model.dart';

class OrderListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Consumer<OrderHistoryDetailModel>(builder: (_, model, __) {
        final order = model.order;
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              RouteList.orderDetail,
              arguments: model,
            );
          },
          child: Container(
            width: size.width,
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, 2),
                  blurRadius: 6,
                )
              ],
            ),
            margin: const EdgeInsets.only(
              top: 15.0,
              left: 15.0,
              right: 15.0,
              bottom: 10.0,
            ),
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: 10.0, top: 10.0, right: 15.0),
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0),
                      ),
                      color: Theme.of(context).backgroundColor,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (order.lineItems.isNotEmpty &&
                            order.lineItems[0].featuredImage != null)
                          Stack(
                            children: [
                              const SizedBox(width: 92, height: 86),
                              if (order.lineItems.length > 1)
                                Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: Opacity(
                                    opacity: 0.6,
                                    child: Hero(
                                      tag: 'image-' +
                                          order.id! +
                                          order.lineItems[1].productId!,
                                      child: SizedBox(
                                        height: 75,
                                        width: 80,
                                        child: ImageTools.image(
                                          url: order
                                                  .lineItems[1].featuredImage ??
                                              kDefaultImage,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              Positioned(
                                left: 0,
                                top: 0,
                                child: Hero(
                                  tag: 'image-' +
                                      order.id! +
                                      order.lineItems[0].productId!,
                                  child: SizedBox(
                                    width: 85,
                                    height: 80,
                                    // decoration: BoxDecoration(
                                    //   borderRadius: BorderRadius.circular(10.0),
                                    //   boxShadow: const [
                                    //     BoxShadow(
                                    //       color: Colors.black12,
                                    //       offset: Offset(0, 2),
                                    //       blurRadius: 2,
                                    //     )
                                    //   ],
                                    // ),

                                    child: ImageTools.image(
                                      height: 100,
                                      width: 100,
                                      url: order.lineItems[0].featuredImage ??
                                          kDefaultImage,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        const SizedBox(width: 10),
                        if (order.lineItems.isNotEmpty)
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(height: 2),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        S.of(context).orderNo +
                                            order.number.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption!
                                            .copyWith(
                                              fontSize: 12.0,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            ),
                                      ),
                                    ),
                                    if (order.status != null)
                                      OrderStatusWidget(
                                        title: S.of(context).status,
                                        detail: order.status ==
                                                    OrderStatus.unknown &&
                                                order.orderStatus != null
                                            ? order.orderStatus
                                            : order.status!.content,
                                      ),
                                  ],
                                ),
                                const SizedBox(height: 12),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        order.lineItems[0].name.toString(),
                                        style: const TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        strutStyle: const StrutStyle(
                                            forceStrutHeight: true,
                                            leading: 0.5),
                                      ),
                                    ),
                                    const SizedBox(width: 32),
                                    Text(
                                      'x' + order.quantity.toString(),
                                      style: const TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w700,
                                      ),
                                      strutStyle: const StrutStyle(
                                          forceStrutHeight: true, leading: 0.5),
                                    )
                                  ],
                                ),
                                const SizedBox(height: 2),
                                // Display empty box if Order Address is null
                                order.billing != null
                                    ? Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${order.billing?.firstName} / ${order.billing?.state}${order.billing?.city}${order.billing?.street}',
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .caption!
                                                  .copyWith(
                                                    fontSize: 14.0,
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .secondary
                                                        .withOpacity(0.8),
                                                  ),
                                            ),
                                          ),
                                          const SizedBox(width: 32),
                                          Text(
                                            'x' + order.quantity.toString(),
                                            style: const TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w700,
                                                color: Colors.transparent),
                                            strutStyle: const StrutStyle(
                                                forceStrutHeight: true,
                                                leading: 0.5),
                                          )
                                        ],
                                      )
                                    : Container(),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
                // Divider(
                //   height: 1,
                //   color: Theme.of(context).primaryColorLight,
                // ),
                const Divider(height: 0.5),

                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    ),
                    color: Theme.of(context).backgroundColor,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      // color: Theme.of(context).primaryColorLight,
                    ),
                    margin: const EdgeInsets.all(8),
                    padding: EdgeInsets.only(
                      right: 7,
                      left: !Tools.isRTL(context) ? 0.0 : 20,
                      top: 8,
                      bottom: 8,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              '日期 : ' +
                                  DateFormat('yyyy-MM-dd HH:mm')
                                      .format(order.createdAt!),
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ],
                        ),
                        // Container(
                        //   width: 10,
                        //   height: 62,
                        //   decoration: BoxDecoration(
                        //     color: Theme.of(context).primaryColor,
                        //     borderRadius: BorderRadius.circular(5.0),
                        //   ),
                        // ),
                        Text(
                          'NT.${order.total?.toInt()}',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        // OrderStatusWidget(
                        //   title: S.of(context).total,
                        //   detail: PriceTools.getCurrencyFormatted(
                        //       order.total, null),
                        // ),
                        // OrderStatusWidget(
                        //   title: S.of(context).tax,
                        //   detail: PriceTools.getCurrencyFormatted(
                        //       order.totalTax, null),
                        // ),
                        // OrderStatusWidget(
                        //   title: S.of(context).Qty,
                        //   detail: order.quantity.toString(),
                        // ),
                        // if (order.status != null)
                        //   OrderStatusWidget(
                        //     title: S.of(context).status,
                        //     detail: order.status == OrderStatus.unknown &&
                        //             order.orderStatus != null
                        //         ? order.orderStatus
                        //         : order.status!.content,
                        //   ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class OrderStatusWidget extends StatelessWidget {
  final String? title;
  final String? detail;

  const OrderStatusWidget({Key? key, this.title, this.detail})
      : super(key: key);

  String getTitleStatus(String status, BuildContext context) {
    switch (status.toLowerCase()) {
      case 'onhold':
        return S.of(context).orderStatusOnHold;
      case 'pending':
        return S.of(context).orderStatusPendingPayment;
      case 'failed':
        return S.of(context).orderStatusFailed;
      case 'processing':
        return S.of(context).orderStatusProcessing;
      case 'completed':
        return S.of(context).orderStatusCompleted;
      case 'cancelled':
        return S.of(context).orderStatusCancelled;
      case 'refunded':
        return S.of(context).orderStatusRefunded;
      default:
        return status;
    }
  }

  @override
  Widget build(BuildContext context) {
    var statusOrderColor = Colors.black;
    switch (detail!.toLowerCase()) {
      case 'pending':
        {
          statusOrderColor = Colors.red;
          break;
        }
      case 'processing':
        {
          statusOrderColor = Colors.orange;
          break;
        }
      case 'completed':
        {
          statusOrderColor = Colors.green;
          break;
        }
    }

    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: statusOrderColor),
          borderRadius: BorderRadius.circular(5)),
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
      child: Text(
        getTitleStatus(detail!, context).capitalize(),
        style: Theme.of(context).textTheme.subtitle1!.copyWith(
              color: statusOrderColor,
              fontWeight: FontWeight.w700,
              fontSize: 14.0,
            ),
        overflow: TextOverflow.ellipsis,
      ),
    );

    // return Expanded(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     children: [
    //       const SizedBox(
    //         height: 10.0,
    //       ),
    //       Expanded(
    //         child: Text(
    //           title.toString(),
    //           style: Theme.of(context)
    //               .textTheme
    //               .caption!
    //               .copyWith(
    //                 fontSize: 14.0,
    //                 color: Theme.of(context)
    //                     .colorScheme
    //                     .secondary
    //                     .withOpacity(0.7),
    //                 fontWeight: FontWeight.w700,
    //               )
    //               .apply(fontSizeFactor: 0.9),
    //         ),
    //       ),
    //       Expanded(
    //         child: Text(
    //           getTitleStatus(detail!, context).capitalize(),
    //           style: Theme.of(context).textTheme.subtitle1!.copyWith(
    //                 color: statusOrderColor,
    //                 fontWeight: FontWeight.w700,
    //                 fontSize: 14.0,
    //               ),
    //           overflow: TextOverflow.ellipsis,
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
