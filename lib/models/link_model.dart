import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../common/constants.dart';
import '../env.dart';

class LinkModel {
  String shopDEVUrl = 'https://majorbiotechshop.31app.tw';
  String shopUATUrl = 'https://majorbiotechshop-uat.31app.tw';
  String memberUrl = 'https://majorbiotechsmember.31app.tw';
  String privateUrl =
      'https://majorbiotechshop.31app.tw/privacy-policy/?appstyle=1';
  String contactUsUrl =
      'https://majorbiotechshop.31app.tw/contactus/?appstyle=1';
  String orderAskUrl =
      'https://majorbiotechshop.31app.tw/return/?appstyle=1&order_number=';
  String healthReserveUrl = 'https://majorbiotechreserve.31app.tw/?uuid=';
  String unLoginReserveUrl = 'https://majorbiotechreserve.31app.tw/guest';
  String purchaseInfoUrl =
      'https://majorbiotechshop.31app.tw/%e8%b3%bc%e7%89%a9%e9%9c%80%e7%9f%a5/?appstyle=1';
  String healthCloudUrl =
      'http://4fd6-159-223-44-239.ap.ngrok.io/gconnect?token=';
  String aboutUsUrl =
      'https://majorbiotechshop.31app.tw/%e9%97%9c%e6%96%bc%e6%88%91%e5%80%91';
  String paymentUrl =
      'https://majorbiotechshop.31app.tw/wc-api/tradevan_payment/?order_id=';

  Future<void> getUrl(String version) async {
    try {
      final response = await httpPost(
        'http://majorbiotechssetting.31app.tw/api/vc/currentVersion'.toUri()!,
        body: {
          'version': version,
        },
        enableDio: true,
      );
      final body = convert.jsonDecode(response.body);
      for (var element in body['data']) {
        ///判斷是否為UAT
        if (element['name'] == '商城DEV' && !environment['UAT']) {
          environment['serverConfig']['url'] =
              urlConvert(element['link']) ?? shopDEVUrl;
        }
        if (element['name'] == '商城UAT' && environment['UAT']) {
          environment['serverConfig']['url'] =
              urlConvert(element['link']) ?? shopUATUrl;
        }
        if (element['name'] == '會員平台') {
          environment['serverConfig']['memberUrl'] =
              urlConvert(element['link']) ?? memberUrl;
        }
        if (element['name'] == '隱私權政策') {
          environment['PrivacyPoliciesPageUrl'] =
              urlConvert(element['link']) ?? privateUrl;
        }
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> checkVersion(BuildContext context, String version) async {
    try {
      final response = await httpGet(
        'http://majorbiotechssetting.31app.tw/api/vc/check?version=$version'
            .toUri()!,
        enableDio: true,
      );
      final body = convert.jsonDecode(response.body);
      if (body['data']['update'] ?? false) {
        unawaited(
          showDialog(
            context: context,
            useRootNavigator: false,
            builder: (BuildContext context) {
              return AlertDialog(
                content: const Text('有新版本'),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('取消'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      if (Platform.isAndroid) {
                        if (body['data']['android_link'] != null) {
                          launch(body['data']['android_link']);
                        }
                      }
                      if (Platform.isIOS) {
                        if (body['data']['ios_link'] != null) {
                          launch(body['data']['ios_link']);
                        }
                      }
                    },
                    child: const Text(
                      '更新',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        );
      }
    } catch (e) {
      rethrow;
    }
  }

  String? urlConvert(String? url) {
    if (url != null && url.isNotEmpty) {
      if (url[url.length - 1] == '/') {
        return url.substring(0, url.length - 1);
      }
    }
    return url;
  }
}
