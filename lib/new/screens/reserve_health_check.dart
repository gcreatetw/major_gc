import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fstore/common/config.dart';
import 'package:fstore/common/constants.dart';
import 'package:fstore/models/index.dart';
import 'package:fstore/widgets/common/webview.dart';
import 'package:fstore/widgets/common/webview_load_error.dart';
import 'package:inspireui/utils/encode.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart' as flutter;

import '../../app.dart';
import '../../env.dart';

class ReserveHealthCheckScreen extends StatefulWidget {
  const ReserveHealthCheckScreen({Key? key}) : super(key: key);

  @override
  _ReserveHealthCheckScreenState createState() =>
      _ReserveHealthCheckScreenState();
}

class _ReserveHealthCheckScreenState extends State<ReserveHealthCheckScreen> {
  late flutter.WebViewController _controller;
  var loadError = false;
  int currentIndex = 1;
  bool isLogin = false;

  Future<void> handleUrlChanged(String url) async {
    final userModel = Provider.of<UserModel>(context, listen: false);
    if (url.contains(environment['serverConfig']['url'])) {
      setState(() {
        _controller.loadUrl(
            'https://majorbiotechreserve.31app.tw/?uuid=${userModel.user!.uuid!}');
        isLogin = true;
      });
    }
    if (url.contains('https://majorbiotechreserve.31app.tw/login') ||
        url.contains('https://majorbiotechreserve.31app.tw/register')) {
      setState(() {
        currentIndex = 1;
      });
      unawaited(
        _controller.loadUrl('https://majorbiotechreserve.31app.tw/guest'),
      );
      await Navigator.of(
        App.fluxStoreNavigatorKey.currentContext!,
      ).pushNamed(RouteList.login);
      if (userModel.user?.uuid != null) {
        await _controller.loadUrl(
            'https://majorbiotechreserve.31app.tw/?uuid=${userModel.user!.uuid!}');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<UserModel>(context);
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          body: SafeArea(
            child: IndexedStack(
              index: currentIndex,
              children: [
                flutter.WebView(
                  initialUrl: userModel.user?.uuid == null
                      ? 'https://majorbiotechreserve.31app.tw/guest'
                      : 'https://majorbiotechreserve.31app.tw/?uuid=${userModel.user!.uuid!}',
                  javascriptMode: flutter.JavascriptMode.unrestricted,
                  onWebViewCreated: (webViewController) {
                    _controller = webViewController;
                  },
                  gestureNavigationEnabled: true,
                  onProgress: (progress) async {
                    if (progress == 100 && !loadError) {
                      setState(() {
                        currentIndex = 0;
                      });
                      return;
                    }
                    if (progress != 100 && !loadError) {
                      setState(() {
                        currentIndex = 1;
                      });
                    }
                  },
                  onPageFinished: handleUrlChanged,
                  onWebResourceError: (e) {
                    setState(() {
                      loadError = true;
                      currentIndex = 2;
                    });
                  },
                ),
                Center(
                  child: kLoadingWidget(context),
                ),
                const WebViewLoadError(),
              ],
            ),
          ),
        ),
        onWillPop: () async {
          var value = await _controller.canGoBack();
          if (value) {
            await _controller.goBack();
            return false;
          } else {
            return true;
          }
        });
  }
}
