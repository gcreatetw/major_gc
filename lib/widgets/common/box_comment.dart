import 'package:flutter/material.dart';

class BoxComment extends CustomPainter {
  final Color? color;

  BoxComment({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = color!;
    var path = Path();
    path.moveTo(0, size.height/2);
    path.lineTo(10, size.height/2-5);
    path.lineTo(10, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(10, size.height);
    path.lineTo(10, size.height/2+5);
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
