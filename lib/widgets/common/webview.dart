import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fstore/custom/models/mj_user_info_model.dart';
import 'package:fstore/models/user_model.dart';
import 'package:fstore/widgets/common/webview_load_error.dart';
import 'package:html/parser.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart' as flutter;
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../html/index.dart';
import 'webview_inapp.dart';
import 'webview_window.dart';

class WebView extends StatefulWidget {
  final String? url;
  final String? title;
  final AppBar? appBar;
  final bool enableForward;
  final bool? isLogin;
  final bool? noAppBar;

  const WebView(
      {Key? key,
      this.title,
      required this.url,
      this.appBar,
      this.enableForward = false,
      this.isLogin,
      this.noAppBar})
      : super(key: key);

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  bool isLoading = true;
  int currentIndex = 1;
  String html = '';
  late flutter.WebViewController _controller;
  var loadError = false;

  @override
  void initState() {
    if (isMacOS) {
      httpGet(widget.url.toString().toUri()!).then((response) {
        setState(() {
          html = response.body;
        });
      });
    }

    if (isAndroid) flutter.WebView.platform = flutter.SurfaceAndroidWebView();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isMacOS) {
      return Scaffold(
        appBar: widget.appBar ??
            AppBar(
              backgroundColor: Theme.of(context).backgroundColor,
              elevation: 0.0,
              title: Center(
                child: Text(
                  widget.title ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
            ),
        body: SingleChildScrollView(
          child: HtmlWidget(html),
        ),
      );
    }

    if (isWindow) {
      return WebViewWindow(url: widget.url!, title: widget.title);
    }

    /// is Mobile or Web
    if (!kIsWeb && (kAdvanceConfig['inAppWebView'] ?? false)) {
      return WebViewInApp(url: widget.url!, title: widget.title);
    }

    return Scaffold(
      appBar: widget.noAppBar == true
          ? null
          : widget.appBar ??
              AppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                elevation: 0.0,
                title: Text(
                  widget.title ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
                leading: Builder(builder: (buildContext) {
                  return Row(
                    children: [
                      IconButton(
                        icon: const Icon(Icons.arrow_back_ios),
                        onPressed: () async {
                          var value = await _controller.canGoBack();
                          if (value) {
                            await _controller.goBack();
                          } else if (Navigator.canPop(context)) {
                            Navigator.of(context).pop();
                          } else {
                            Tools.showSnackBar(Scaffold.of(buildContext),
                                S.of(context).noBackHistoryItem);
                          }
                        },
                      ),
                      if (widget.enableForward)
                        IconButton(
                          onPressed: () async {
                            if (await _controller.canGoForward()) {
                              await _controller.goForward();
                            } else {
                              Tools.showSnackBar(Scaffold.of(buildContext),
                                  S.of(context).noForwardHistoryItem);
                            }
                          },
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                    ],
                  );
                }),
              ),
      body: IndexedStack(
        index: currentIndex,
        children: [
          Builder(builder: (BuildContext context) {
            return flutter.WebView(
              initialUrl: widget.url!,
              javascriptMode: flutter.JavascriptMode.unrestricted,
              onWebViewCreated: (webViewController) {
                _controller = webViewController;
                if (widget.isLogin ?? false) {
                  _controller.clearCache();
                  final cookieManager = CookieManager();
                  cookieManager.clearCookies();
                }
              },
              onPageFinished: (_) {
                setState(() {
                  currentIndex = 0;
                });
                _controller
                    .runJavascriptReturningResult(
                        'document.documentElement.outerHTML')
                    .then((html) async {
                  try {
                    var document = parse(isAndroid ? jsonDecode(html) : html);
                    if (document.body != null) {
                      if (document.body!.text.contains('uuid')) {
                        setState(() {
                          currentIndex = 1;
                        });
                        final userModel =
                            Provider.of<UserModel>(context, listen: false);
                        var loggedIn = await userModel.getUserBySSO(
                            jsonDecode(document.body!.text)['uuid']);
                        userModel.user?.uuid =
                            jsonDecode(document.body!.text)['uuid'];
                        await userModel.saveUser(userModel.user);
                        if (loggedIn) {
                          await Provider.of<MJUserInfo>(context, listen: false)
                              .fetch(identity: userModel.user!.identity!);
                        }
                        Navigator.pop(context);
                      }
                    }
                  } catch (e) {
                    rethrow;
                  }
                });
              },
              onProgress: (progress) async {
                print(progress);
                var showLoading = false;
                await _controller
                    .runJavascriptReturningResult(
                        'document.documentElement.outerHTML')
                    .then((html) async {
                  try {
                    var document = parse(isAndroid ? jsonDecode(html) : html);
                    if (document.body != null &&
                        document.body!.text.contains('uuid')) {
                      showLoading = true;
                    }
                  } catch (e) {
                    rethrow;
                  }
                });
                if (progress == 100 && !showLoading && !loadError) {
                  setState(() {
                    currentIndex = 0;
                  });
                  return;
                }
                if (progress != 100 || showLoading && !loadError) {
                  setState(() {
                    currentIndex = 1;
                  });
                }
              },
              onWebResourceError: (e) {
                setState(() {
                  loadError = true;
                  currentIndex = 2;
                });
              },
              gestureNavigationEnabled: true,
            );
          }),
          Center(
            child: kLoadingWidget(context),
          ),
          const WebViewLoadError(),
        ],
      ),
    );
  }
}
