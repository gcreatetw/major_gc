import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/material.dart';

class WebViewLoadError extends StatefulWidget {
  const WebViewLoadError({Key? key}) : super(key: key);

  @override
  State<WebViewLoadError> createState() => _WebViewLoadErrorState();
}

class _WebViewLoadErrorState extends State<WebViewLoadError> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: ((MediaQuery.of(context).size.width - 24) / 4 * 3) / 2,
          child: const FlareActor('assets/images/no_internet.flr'),
        ),
        const SizedBox(height: 8),
        const Text('無網路服務'),
      ],
    );
  }
}
