import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../common/tools/adaptive_tools.dart';
import '../../models/entities/blog.dart';
import '../../modules/dynamic_layout/config/blog_config.dart';
import '../common/flux_image.dart';

class BlogGridItem extends StatelessWidget {
  final Blog blog;
  final VoidCallback onTap;
  final double radius;
  final double innerPadding;
  final Color background;
  final Color itemBackgroundColor;
  final BlogConfig config;

  const BlogGridItem({
    required this.blog,
    required this.onTap,
    this.radius = 0.0,
    this.innerPadding = 0.0,
    this.background = Colors.transparent,
    this.itemBackgroundColor = Colors.transparent,
    required this.config,
  });

  @override
  Widget build(BuildContext context) {
    var offSet = 11.0;
    if (config.hideComment && config.hideAuthor && config.hideComment) {
      offSet -= 11.0;
    }
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: getValueForScreenType<double>(
          context: context,
          mobile: 260 + innerPadding + offSet,
          tablet: 120 + innerPadding + offSet,
          desktop: 180 + innerPadding + offSet,
        ),
      ),
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.only(
            bottom: 6,
            left: context.isRtl ? 16 : 0,
            right: context.isRtl ? 0 : 16,
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(radius),
              color: itemBackgroundColor,
            ),
            padding: EdgeInsets.all(innerPadding),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 14,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      child: FluxImage(
                        imageUrl: blog.imageFeature,
                        fit: BoxFit.fitWidth,
                        width: double.maxFinite,
                        // isVideo: blog.videoUrl.isNotEmpty,
                      ),
                    ),
                  ),
                  SizedBox(width: innerPadding),
                  Expanded(
                    flex: 8,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(height: 4),
                        if (!config.hideTitle)
                          Text(
                            blog.title,
                            maxLines: 2,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        const SizedBox(width: 13),
                        Wrap(
                          children: [
                            if (!config.hideDate && blog.date.isNotEmpty)
                              Text(
                                blog.date,
                                style: TextStyle(
                                  fontSize: 11,
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                ),
                              ),
                            if (!config.hideAuthor && blog.author.isNotEmpty)
                              Padding(
                                padding: EdgeInsets.only(
                                    left: config.hideDate ? 0 : 8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(
                                      Icons.person,
                                      size: 11,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                    ),
                                    const SizedBox(width: 2.0),
                                    Text(
                                      blog.author,
                                      style: TextStyle(
                                        fontSize: 11,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            if (!config.hideComment)
                              Padding(
                                padding: EdgeInsets.only(
                                    left: (config.hideAuthor && config.hideDate)
                                        ? 0
                                        : 8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(
                                      Icons.message_outlined,
                                      size: 11,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                    ),
                                    const SizedBox(width: 2.0),
                                    Text(
                                      '${blog.commentCount}',
                                      style: TextStyle(
                                        fontSize: 11,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                        Text(
                          blog.subTitle
                              .replaceAll('<p>', '')
                              .replaceAll('</p>', ''),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
