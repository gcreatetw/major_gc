import 'package:flutter/material.dart';
import 'package:fstore/modules/dynamic_layout/config/banner_config.dart';
import 'package:fstore/services/services.dart';

import 'general_state.dart';

class BannerData extends ChangeNotifier {
  List<BannerItemConfig> bannerList = [];
  GeneralState state = GeneralState.loading;

  final Services _service = Services();

  BannerItemConfig fromJson(Map<String, dynamic>? json) {
    return BannerItemConfig(image: json!['image'],url: json['link']);
  }

  Future<void> fetch() async {
    bannerList = [];
    state = GeneralState.loading;
    notifyListeners();
    try {
      bannerList = (await _service.api.getBanner())!;
      state = GeneralState.finish;
      notifyListeners();
    } catch (e) {
      state = GeneralState.error;
      notifyListeners();
    }
  }
}
