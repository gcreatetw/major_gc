import 'package:flutter/material.dart';
import 'package:fstore/custom/models/general_state.dart';
import 'package:fstore/services/services.dart';

class MJUserInfo extends ChangeNotifier {
  MJUserInfoModel? mjUserInfoModel;

  GeneralState state = GeneralState.loading;

  int? cardIndex;

  final Services _service = Services();

  Future<void> fetch({required String identity}) async {
    state = GeneralState.loading;
    notifyListeners();
    try {
      mjUserInfoModel = await _service.api.getMJUserInfo(identity: identity);

      state = GeneralState.finish;
      notifyListeners();
    } catch (e) {
      state = GeneralState.error;
      notifyListeners();
    }
  }

  void logout() {
    mjUserInfoModel = null;
    notifyListeners();
  }

  void setCardIndex(int value) {
    cardIndex = value;
    notifyListeners();
  }
}

class MJUserInfoModel {
  String? jobType;
  String? idno;
  String? birthday;
  String? cardnm;
  String? cardfn;
  String? gno;
  String? telo;
  String? telh;
  String? telb;
  String? fax1;
  String? email;
  String? language;
  String? sign5;
  String? sign1;
  String? sign4;
  String? copno;
  String? copnm;
  String? hcdate1;
  List<AddressMJ>? address;
  List<Cardtype>? cardtype;

  MJUserInfoModel(
      {this.jobType,
      this.idno,
      this.birthday,
      this.cardnm,
      this.cardfn,
      this.gno,
      this.telo,
      this.telh,
      this.telb,
      this.fax1,
      this.email,
      this.language,
      this.sign5,
      this.sign1,
      this.sign4,
      this.copno,
      this.copnm,
      this.hcdate1,
      this.address,
      this.cardtype});

  MJUserInfoModel.fromJson(Map<String, dynamic> json) {
    jobType = json['job_type'];
    idno = json['idno'];
    birthday = json['birthday'];
    cardnm = json['cardnm'];
    cardfn = json['cardfn'];
    gno = json['gno'];
    telo = json['telo'];
    telh = json['telh'];
    telb = json['telb'];
    fax1 = json['fax1'];
    email = json['email'];
    language = json['language'];
    sign5 = json['sign5'];
    sign1 = json['sign1'];
    sign4 = json['sign4'];
    copno = json['copno'];
    copnm = json['copnm'];
    hcdate1 = json['hcdate1'];
    if (json['address'] != null) {
      address = <AddressMJ>[];
      json['address'].forEach((v) {
        address!.add(AddressMJ.fromJson(v));
      });
    }
    if (json['cardtype'] != null) {
      cardtype = <Cardtype>[];
      json['cardtype'].forEach((v) {
        cardtype!.add(Cardtype.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['job_type'] = jobType;
    data['idno'] = idno;
    data['birthday'] = birthday;
    data['cardnm'] = cardnm;
    data['cardfn'] = cardfn;
    data['gno'] = gno;
    data['telo'] = telo;
    data['telh'] = telh;
    data['telb'] = telb;
    data['fax1'] = fax1;
    data['email'] = email;
    data['language'] = language;
    data['sign5'] = sign5;
    data['sign1'] = sign1;
    data['sign4'] = sign4;
    data['copno'] = copno;
    data['copnm'] = copnm;
    data['hcdate1'] = hcdate1;
    if (address != null) {
      data['address'] = address!.map((v) => v.toJson()).toList();
    }
    if (cardtype != null) {
      data['cardtype'] = cardtype!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AddressMJ {
  String? idno;
  String? addresstype;
  String? country;
  String? townno;
  String? city;
  String? zip;
  String? blocknm;
  String? addr;

  AddressMJ(
      {this.idno,
      this.addresstype,
      this.country,
      this.townno,
      this.city,
      this.zip,
      this.blocknm,
      this.addr});

  AddressMJ.fromJson(Map<String, dynamic> json) {
    idno = json['idno'];
    addresstype = json['addresstype'];
    country = json['country'];
    townno = json['townno'];
    city = json['city'];
    zip = json['zip'];
    blocknm = json['blocknm'];
    addr = json['addr'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['idno'] = idno;
    data['addresstype'] = addresstype;
    data['country'] = country;
    data['townno'] = townno;
    data['city'] = city;
    data['zip'] = zip;
    data['blocknm'] = blocknm;
    data['addr'] = addr;
    return data;
  }
}

class Cardtype {
  String? custno;
  String? famno;
  String? cardtype;
  String? cardholder;
  String? cardholderbirthday;
  String? copno;
  String? copnm;

  Cardtype({
    this.custno,
    this.famno,
    this.cardtype,
    this.cardholder,
    this.cardholderbirthday,
    this.copno,
    this.copnm,
  });

  Cardtype.fromJson(Map<String, dynamic> json) {
    custno = json['custno'];
    famno = json['famno'];
    cardtype = json['cardtype'];
    cardholder = json['cardholder'];
    cardholderbirthday = json['cardholderbirthday'];
    copno = json['copno'];
    copnm = json['copnm'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['custno'] = custno;
    data['famno'] = famno;
    data['cardtype'] = cardtype;
    data['cardholder'] = cardholder;
    data['cardholderbirthday'] = cardholderbirthday;
    data['copnm'] = copnm;
    data['copno'] = copno;
    return data;
  }
}
