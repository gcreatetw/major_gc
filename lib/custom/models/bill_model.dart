import 'package:flutter/material.dart';

class BillModel extends ChangeNotifier {
  String? billType;
  String? uniCode;
  String? companyName;
  String? carrierType;
  String? natureCode;
  String? phoneCode;

  void setBillInfo(BillModel billModel) {
    billType = billModel.billType;
    uniCode = billModel.uniCode;
    companyName = billModel.companyName;
    carrierType = billModel.carrierType;
    natureCode = billModel.natureCode;
    phoneCode = billModel.phoneCode;
    notifyListeners();
  }

  Map<String, dynamic> toJson(BillModel billModel) {
    var json = {
      'billType': billModel.billType,
      'uniCode': billModel.uniCode,
      'companyName': billModel.companyName,
      'carrierType': billModel.carrierType,
      'natureCode': billModel.natureCode,
      'phoneCode': billModel.phoneCode,
    };
    print(json);
    return json;
  }

  BillModel fromJson(Map<String, dynamic> json) {
    var bill = BillModel();
    bill.billType = json['billType'];
    bill.uniCode = json['uniCode'];
    bill.companyName = json['companyName'];
    bill.carrierType = json['carrierType'];
    bill.natureCode = json['natureCode'];
    bill.phoneCode = json['phoneCode'];
    print(json);
    return bill;
  }
}
