import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fstore/widgets/common/webview_load_error.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../app.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../models/index.dart';
import '../../routes/flux_navigate.dart';
import '../../screens/cart/cart_screen.dart';
import '../../services/index.dart';
import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider.dart';
import 'blog/blog_grid.dart';
import 'brand/brand_layout.dart';
import 'button/button.dart';
import 'category/category_icon.dart';
import 'category/category_image.dart';
import 'category/category_menu_with_products.dart';
import 'category/category_text.dart';
import 'config/brand_config.dart';
import 'config/index.dart';
import 'divider/divider.dart';
import 'header/header_search.dart';
import 'header/header_text.dart';
import 'instagram_story/instagram_story.dart';
import 'logo/logo.dart';
import 'product/product_list_simple.dart';
import 'product/product_recent_placeholder.dart';
import 'slider_testimonial/index.dart';
import 'spacer/spacer.dart';
import 'story/index.dart';
import 'testimonial/index.dart';
import 'video/index.dart';

class DynamicLayout extends StatelessWidget {
  final config;
  final bool cleanCache;

  const DynamicLayout({this.config, this.cleanCache = false});

  @override
  Widget build(BuildContext context) {
    final appModel = Provider.of<AppModel>(context, listen: true);

    switch (config['layout']) {
      case 'spacer':
        return SizedBox(
          height: config['height'],
          width: config['width'],
        );
      case 'customHeader':
        return const HomePageBanner();

      ///custom homepage layout
      case 'customCard':
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Card(
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Image.asset(config['imagePath']),
            ),
          ),
        );
      case 'imageCategory':
        List images = config['images'];
        var categories = Provider.of<CategoryModel>(context).categories;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                for (int i = 0; i < images.length; i++)
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        if (categories == null ||
                            !categories.any((element) =>
                                element.id ==
                                images[i]['category'].toString())) {
                          //TODO error message
                          return;
                        }
                        FluxNavigate.pushNamed(
                          RouteList.backdrop,
                          arguments: BackDropArguments(
                            config: CategoryItemConfig(
                                    category: images[i]['category'],
                                    name: categories
                                        .where((element) =>
                                            element.id ==
                                            images[i]['category'].toString())
                                        .single
                                        .name)
                                .toJson(),
                          ),
                        );
                      },
                      child: FittedBox(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                                right: i == 0
                                    ? const BorderSide(
                                        color: Colors.black12, width: 0.5)
                                    : BorderSide.none,
                                left: i != 0
                                    ? const BorderSide(
                                        color: Colors.black12, width: 0.5)
                                    : BorderSide.none),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(i == 0 ? 10 : 0),
                                  topLeft: Radius.circular(i == 0 ? 10 : 0),
                                  bottomRight: Radius.circular(i != 0 ? 10 : 0),
                                  topRight: Radius.circular(i != 0 ? 10 : 0)),
                            ),
                            padding: const EdgeInsets.all(20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: 55,
                                  width: 55,
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      color: HexColor('#67BCB0'),
                                      borderRadius: BorderRadius.circular(55)),
                                  child: Image.asset(images[i]['image']),
                                ),
                                const SizedBox(width: 8),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      images[i]['title'],
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                    Text(
                                      images[i]['subTitle'],
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: HexColor('#67BCB0'),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      case 'logo':
        final themeConfig = appModel.themeConfig;
        return Logo(
          config: LogoConfig.fromJson(config),
          logo: themeConfig.logo,
          totalCart:
              Provider.of<CartModel>(context, listen: true).totalCartQuantity,
          notificationCount:
              Provider.of<NotificationModel>(context).unreadCount,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          onCheckout: () {
            Navigator.push(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) => Scaffold(
                  backgroundColor: Theme.of(context).backgroundColor,
                  body: const CartScreen(isModal: true),
                ),
                fullscreenDialog: true,
              ),
            );
          },
          onTapNotifications: () {
            Navigator.of(context).pushNamed(RouteList.notify);
          },
          onTapDrawerMenu: () => NavigateTools.onTapOpenDrawerMenu(context),
        );

      case 'header_text':
        return HeaderText(
          config: HeaderConfig.fromJson(config),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'header_search':
        return HeaderSearch(
          config: HeaderConfig.fromJson(config),
          onSearch: () {
            Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
                .pushNamed(RouteList.homeSearch);
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'featuredVendors':
        return Services().widget.renderFeatureVendor(config);
      case 'category':
        if (config['type'] == 'image') {
          return CategoryImages(
            config: CategoryConfig.fromJson(config),
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        }
        return Consumer<CategoryModel>(builder: (context, model, child) {
          var _config = CategoryConfig.fromJson(config);
          var _listCategoryName =
              model.categoryList.map((key, value) => MapEntry(key, value.name));
          void _onShowProductList(CategoryItemConfig item) {
            FluxNavigate.pushNamed(
              RouteList.backdrop,
              arguments: BackDropArguments(
                config: item.jsonData,
                data: item.data,
              ),
            );
          }

          if (config['type'] == 'menuWithProducts') {
            return CategoryMenuWithProducts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          if (config['type'] == 'text') {
            return CategoryTexts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          return CategoryIcons(
            config: _config,
            listCategoryName: _listCategoryName,
            onShowProductList: _onShowProductList,
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        });
      // case 'bannerAnimated':
      //   if (kIsWeb) return const SizedBox();
      //   return BannerAnimated(
      //     config: BannerConfig.fromJson(config),
      //     key: config['key'] != null ? Key(config['key']) : UniqueKey(),
      //   );

      case 'bannerImage':
        if (config['isSlider'] == true) {
          return Padding(
            padding:
                const EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 8),
            child: Card(
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: BannerSlider(
                    config: BannerConfig.fromJson(config),
                    onTap: (itemConfig) {
                      NavigateTools.onTapNavigateOptions(
                        context: context,
                        config: itemConfig,
                      );
                    },
                    key: config['key'] != null
                        ? Key(config['key'])
                        : UniqueKey()),
              ),
            ),
          );
        }

        return BannerGroupItems(
          config: BannerConfig.fromJson(config),
          onTap: (itemConfig) {
            NavigateTools.onTapNavigateOptions(
              context: context,
              config: itemConfig,
            );
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'blog':
        return BlogGrid(
          config: BlogConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'video':
        return VideoLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'story':
        return StoryWidget(
          config: config,
          onTapStoryText: (cfg) {
            NavigateTools.onTapNavigateOptions(context: context, config: cfg);
          },
        );
      case 'recentView':
        if (Config().isBuilder) {
          return ProductRecentPlaceholder();
        }
        return Services().widget.renderHorizontalListItem(config);
      case 'fourColumn':
      case 'threeColumn':
      case 'twoColumn':
      case 'staggered':
      case 'saleOff':
      case 'card':
      case 'listTile':
        return Services().widget.renderHorizontalListItem(config,
            cleanCache: cleanCache, fit: true);

      /// New product layout style.
      case 'largeCardHorizontalListItems':
      case 'largeCard':
        return Services().widget.renderLargeCardHorizontalListItems(config);
      case 'simpleVerticalListItems':
      case 'simpleList':
        return SimpleVerticalProductList(
          config: ProductConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'brand':
        return BrandLayout(
          config: BrandConfig.fromJson(config),
        );

      /// FluxNews
      case 'sliderList':
        return Services().widget.renderSliderList(config);
      case 'sliderItem':
        return Services().widget.renderSliderItem(config);

      case 'geoSearch':
        return Services().widget.renderGeoSearch(config);
      case 'divider':
        return DividerLayout(
          config: DividerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'spacer':
        return SpacerLayout(
          config: SpacerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'button':
        return ButtonLayout(
          config: ButtonConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'testimonial':
        return TestimonialLayout(
          config: TestimonialConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'sliderTestimonial':
        return SliderTestimonial(
          config: SliderTestimonialConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'instagramStory':
        return InstagramStory(
          config: InstagramStoryConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      default:
        return const SizedBox();
    }
  }
}

class HomePageBanner extends StatefulWidget {
  const HomePageBanner({Key? key}) : super(key: key);

  @override
  State<HomePageBanner> createState() => _HomePageBannerState();
}

class _HomePageBannerState extends State<HomePageBanner> {
  var loadError = false;

  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<UserModel>(context);

    var now = DateTime.now();
    var welcomeText = '早安';
    if (now.hour >= 5 && now.hour < 11) {
      welcomeText = '早安';
    }
    if (now.hour >= 11 && now.hour < 17) {
      welcomeText = '午安';
    }
    if (now.hour >= 17 || now.hour < 5) {
      welcomeText = '晚安';
    }
    return Column(
      children: [
        const SizedBox(height: 12),
        // if(!Provider.of<UserModel>(context).loggedIn)
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Provider.of<UserModel>(context).loggedIn
                ? Container(
                    height: (MediaQuery.of(context).size.width - 24) / 4 * 3,
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: loadError
                        ? const WebViewLoadError()
                        : ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            child: WebView(
                              initialUrl:
                                  'http://4fd6-159-223-44-239.ap.ngrok.io/gconnect?token=${userModel.user?.uuid}',
                              javascriptMode: JavascriptMode.unrestricted,
                              zoomEnabled: false,
                              gestureRecognizers: Set()
                                ..add(const Factory<
                                        VerticalDragGestureRecognizer>(
                                    VerticalDragGestureRecognizer.new)),
                              onWebResourceError: (e) {
                                setState(() {
                                  loadError = true;
                                });
                              },
                            ),
                          ),
                  )
                : SizedBox(
                    height: (MediaQuery.of(context).size.width - 24) / 4 * 3,
                    width: double.maxFinite,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const SizedBox(
                          width: double.maxFinite,
                        ),
                        const SizedBox(height: 16),
                        Text(
                          '美兆的好朋友，$welcomeText',
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 6),
                        Text(
                          '請別忘了先登入\n讓美兆替您提供客製化精準健康服務\nCustomized Health Management',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: HexColor('#555555'),
                          ),
                        ),
                        const SizedBox(height: 6),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(
                              App.fluxStoreNavigatorKey.currentContext!,
                            ).pushNamed(RouteList.login);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 60, vertical: 8),
                            decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.circular(50)),
                            child: const Text(
                              '登入',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                      ],
                    ),
                  ),
          ),
        ),
        const SizedBox(height: 12),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Stack(
              alignment: Alignment.center,
              children: [
                const SizedBox(
                  width: double.maxFinite,
                ),
                FittedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(width: 8),
                      Container(
                        alignment: Alignment.centerRight,
                        width: (MediaQuery.of(context).size.width - 81) / 2,
                        child: FittedBox(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(
                                Icons.phone,
                                color: Theme.of(context).primaryColor,
                              ),
                              const Text(
                                '各地預約健檢專線',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                      Container(
                        height: 60,
                        width: 1,
                        color: Theme.of(context).primaryColor,
                      ),
                      const SizedBox(width: 20),
                      Container(
                        alignment: Alignment.centerLeft,
                        width: (MediaQuery.of(context).size.width - 81) / 2,
                        child: FittedBox(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 12),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: '台北 : ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context)
                                                .primaryColor)),
                                    TextSpan(
                                        text: '02-7725-1177',
                                        style: TextStyle(
                                          color: HexColor('#555555'),
                                        )),
                                  ],
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: '台中 : ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context)
                                                .primaryColor)),
                                    TextSpan(
                                        text: '04-2359-8686',
                                        style: TextStyle(
                                          color: HexColor('#555555'),
                                        )),
                                  ],
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: '高雄 : ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context)
                                                .primaryColor)),
                                    TextSpan(
                                        text: '07-226-2288',
                                        style: TextStyle(
                                          color: HexColor('#555555'),
                                        )),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 12),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        const SizedBox(height: 12),
      ],
    );
  }
}
